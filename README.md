# Empty

## Hosts

* [stable](https://domain.com/)
* [dev](http://localhost:3000/)

## Start

`npm i && npm start`

## Linter

`npm run lint`
`npm run lint:fix # for fix rules`
