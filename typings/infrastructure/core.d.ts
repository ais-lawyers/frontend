// IS_STABLE –> webpack.base.config.js
// eslint-disable-next-line no-var
declare var IS_STABLE: boolean;
// IS_SERVER –> package.json
// eslint-disable-next-line no-var
declare var IS_SERVER: boolean;

declare module 'core' {
    export interface BaseResponse<T> {
        data: T;
        message: {
            status: 'success' | 'error',
            error?: string,
        };
    }
}
