declare module 'infrastructure/redux' {
    import {Action as ReduxAction} from 'redux';

    export type Action<P, T = any> = ReduxAction<T> & {
        payload?: P;
    };
}
