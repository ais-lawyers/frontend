import fs from 'fs';
import {Helmet} from 'react-helmet';
import serialize from 'serialize-javascript';

import config from '__config/config';

export default ({
    styles = [],
    scripts = [],
    html,
    store,
}) => {
    const helmet = Helmet.renderStatic();

    return `<!doctype html>
        <html lang="ru">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, ya-title=#62abff, ya-dock=fade">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <meta name="google" content="notranslate">

                <meta content="#62abff" name="msapplication-TileColor">
                <!-- Chrome, Firefox OS and Opera -->
                <meta name="theme-color" content="#62abff">
                <!-- Windows Phone -->
                <meta name="msapplication-navbutton-color" content="#62abff">
                <!-- iOS Safari -->
                <meta name="apple-mobile-web-app-capable" content="yes">
                <meta name="apple-mobile-web-app-status-bar-style" content="#62abff">

                ${config.__PROD__ ? '<meta name="cmsmagazine" content="1a28834e9b90f73526b7dd3ebd34a490"/>' : ''}

                <meta property="og:title" content="Автоматизированная система "Адвокаты"">
                <meta property="og:description" content="Учет">

                <title>Автоматизированная система "Адвокаты</title>
                <meta name="description" content="Учет">

                <meta content="AIS lawyers" name="application-name">

                ${helmet.meta.toString()}
                ${helmet.link.toString()}

                <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
                <link rel="icon" type="image/png" href="/dist/favicons/ais-lawyers-favicon.png">

                ${styles.map(style => `<style rel="stylesheet">${fs.readFileSync(`dist/client/${style.file}`, 'utf-8')}</style>`).join('\n')}
            </head>

            <body>
                <div id="root">${html}</div>

                <script>window.__PRELOADED_STATE__ = ${serialize(store.getState()).replace(/</g, '\\\u003c')}</script>
                ${scripts.map(script => `<script src="/dist/${script.file}"></script>`).join('\n')}
            </body>
        </html>`;
};
