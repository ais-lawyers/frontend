const proxy = require('http-proxy-middleware');

const {HOSTS} = require('./consts');

const isStable = IS_STABLE || process.env.IS_STABLE;

const HOST = HOSTS[isStable ? 'stable' : 'testing'];

export default proxy('/public/api/**', {
    https: false,
    target: `http://${HOST}`,
    pathRewrite: {'^/public/api': '/public/api'},
    secure: false,
    onProxyReq: proxyReq => {
        proxyReq.setHeader('Host', HOST);
    },
});
