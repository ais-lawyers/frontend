import cookieParser from 'cookie-parser';
import express from 'express';
import robots from 'express-robots-txt';
import Loadable from 'react-loadable';

import router from './router';

const PORT = 3000;

if (typeof window === 'undefined') {
    (global as any).window = {};
    (global as any).document = {};
}

const server: express.Application = express();
server.disable('x-powered-by');

server.use(cookieParser());

server.use('/dist', express.static('dist/client'));
server.use(robots('www/robots.txt'));

router(server);

Loadable.preloadAll()
    .then(() => {
        server.listen(PORT, () => {
            // eslint-disable-next-line no-console
            console.log(`Running on http://localhost:${PORT}/`);
        });
    })
    .catch(err => {
        // eslint-disable-next-line no-console
        console.log(err);
    });
