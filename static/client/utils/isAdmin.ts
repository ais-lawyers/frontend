import {localStorageService} from '__services/LocalStorageService';
import {UserRole} from '__types/entity/auth/types';

export const isAdmin = () => {
    const role: UserRole = localStorageService.getItem('role');

    return role?.role === 'admin';
}
