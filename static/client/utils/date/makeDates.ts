import moment from 'moment';

import {DEFAULT_SERVER_DATE_FORMAT} from './defaultDateFormat';

export const makePeriod = (date: string) => {
    const arrDate = date.split(',');
    return `${moment(arrDate[0]).format(DEFAULT_SERVER_DATE_FORMAT)},${moment(arrDate[1]).format(DEFAULT_SERVER_DATE_FORMAT)}`;
}

export const makeDate = (date: string) => 
    `${moment(+date).format(DEFAULT_SERVER_DATE_FORMAT)},${moment(+date).format(DEFAULT_SERVER_DATE_FORMAT)}`;

export const makeAllTime = () => 
    `2001.01.01,${moment().format(DEFAULT_SERVER_DATE_FORMAT)}`;

export const makeMounth = () => 
    `${moment().startOf('month').format(DEFAULT_SERVER_DATE_FORMAT)},${moment().endOf('month').format(DEFAULT_SERVER_DATE_FORMAT)}`;