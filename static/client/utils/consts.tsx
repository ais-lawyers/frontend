import {Skeleton} from 'antd';
import React from 'react';

export const SERVER_RESPONSE_SUCCESS = 'success';

export const SERVER_RESPONSE_ERROR = 'error';

export const DEFAULT_RESPONSE_CATCH_MESSAGE = 'Что-то пошло не так';

export const DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE = 'Неполадки сети';

export const SKELETON = <Skeleton active/>;
