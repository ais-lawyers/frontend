import {bound as commonActions} from '__actions';

export const onPageClick = (path: string) => (page: number) => {
    commonActions.router.push(`${path}/${page}`)
}
