import React from 'react';
import {Tag} from 'antd';

import {Roles} from '__types/entity/auth/types'

const mapRoles = {
    admin: {
        title: 'Администратор',
        color: 'cyan',
        id: 1,
    },
    user: {
        title: 'Пользователь',
        color: 'green',
        id: 2,
    }
};

export const ROLES = [
    {
        title: 'Администратор',
        id: 1,
    },
    {
        title: 'Пользователь',
        id: 2,
    },
];

export const makeRolesTags = (role: Roles) => {
    if (role !== 'admin' && role !== 'user') {
        return <Tag color="red">{role}</Tag>
    }

    return <Tag color={mapRoles[role].color}>{mapRoles[role].title}</Tag>
};
