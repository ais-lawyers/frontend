import React from 'react';

import isServer from '__utils/isServerEnvCheker';

export default function renderOnClientSideOnly<O>(
    Component: React.ComponentType<O>,
    props: O & {},
) {
    if (isServer) {
        return null;
    }

    return <Component {...props}/>;
}
