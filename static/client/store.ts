import {routerMiddleware} from 'connected-react-router';
import {StoreOptions} from 'core-store';
import {createBrowserHistory, createMemoryHistory} from 'history';
import {
    applyMiddleware,
    combineReducers,
    compose,
    createStore,
    Middleware,
} from 'redux';
import {createLogger} from 'redux-logger';
import thunkMiddleware from 'redux-thunk';

import config from '__config/config';
import routerReducer from '__reducers/router';
import isServer from '__utils/isServerEnvCheker';

function configureStore(reducers, initialState = {}, options?: StoreOptions) {
    const {isLogger, router} = options || ({} as StoreOptions);

    const history = !isServer
        ? createBrowserHistory()
        : createMemoryHistory({initialEntries: router?.initialEntries || ['/']});

    const middlewares: Middleware[] = [
        thunkMiddleware,
        routerMiddleware(history),
    ];

    if (!config.__PROD__ && isLogger) {
        const logger = createLogger({collapsed: true});
        middlewares.push(logger);
    }

    const store = createStore(
        combineReducers({
            ...reducers,
            router: routerReducer(history),
        }),
        initialState,
        compose(applyMiddleware(...middlewares)),
    );
    store.dispatch({type: '@@redux/INIT'});

    return {store, history};
}

export default configureStore;
