import {EntityAPI} from 'infrastructure/common';
import {BaseResponse} from 'core';

import {CreatePaymentsRequest} from '__types/entity/payments/types';
import {IDType} from '__types/entity/api';
import {paymentsApi} from '__utils/transport';

class PaymentsAPI implements EntityAPI {
    public create = (data: CreatePaymentsRequest) =>
        paymentsApi.post<CreatePaymentsRequest, BaseResponse<null>>('', data)
            .then(response => response);

    public update = (data: CreatePaymentsRequest, id: IDType) =>
        paymentsApi.put<CreatePaymentsRequest, BaseResponse<null>>(`/${id}`, data)
            .then(response => response);
}

export const apiInstance = new PaymentsAPI();
