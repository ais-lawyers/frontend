import {BaseResponse} from 'core';

import {api} from '__utils/transport';

import {SearchRequest} from '__types/search';
import {AutocompleteType} from '__types/autocomplete';

class SearchAPI {
    public search = (searchString: string, type: AutocompleteType) =>
        api.get<SearchRequest, BaseResponse<{data: string[]}>>(`/${type}`, {search: searchString})
            .then(response => response);
}

export const apiInstance = new SearchAPI();
