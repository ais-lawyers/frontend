import {EntityAPI} from 'infrastructure/common';

import {AuthRequest, AuthRequestResponse} from '__types/entity/auth/types';

import {authApi} from '__utils/transport';

import {BaseResponse} from 'core';

class AuthAPI implements EntityAPI {
    public request = (data: AuthRequest): Promise<BaseResponse<AuthRequestResponse>> =>
        authApi.post<AuthRequest, BaseResponse<AuthRequestResponse>>('', data)
            .then(response => response);
}

export const apiInstance = new AuthAPI();
