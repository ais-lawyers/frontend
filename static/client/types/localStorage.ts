export interface LocalStorageItem {
    [key: string]: any;
}

export type LocalStorageItems = LocalStorageItem[];
