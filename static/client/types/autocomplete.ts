export interface AutocompleteItem {
    value: string;
}

export type AutocompleteItems = AutocompleteItem[];

export type AutocompleteType = 'lawyers' | 'defendants' | 'judges' | 'lawyers-educations';

export type AutocompleteStatuses = 'validating' | 'success' | '';
