import {AutocompleteItems} from "./autocomplete";

export interface SearchRequest {
    search: string;
}

export type SerachRequestResponse = AutocompleteItems;
