export interface Case {
    id: number;
    title: string;
}

export type Cases = Case[];

export const CASE_TYPE: Cases = [
    {
        id: 1,
        title: 'Гражданское',
    },
    {
        id: 2,
        title: 'Административное',
    },
    {
        id: 3,
        title: 'Уголовное',
    }
];
