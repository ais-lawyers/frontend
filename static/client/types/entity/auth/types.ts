export interface AuthRequest {
    username: string;
    password: string;
}

export interface UserRole {
    id: number;
    role: Roles;
}

export type Roles = 'admin' | 'user';

export interface User {
    created_at: string;
    id: number;
    role: UserRole;
    updated_at: string;
    username: string;
}

export type Users = User[];

export interface AuthRequestResponse {
    user: User;
    token: string;
}
