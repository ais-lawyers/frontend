export interface CreatePaymentsRequest {
    lawyer: string;
    cash: number;
    lawyer_education: string;
    judge: string;
    date: string;
    defendant: string;
    type_id: number;
    no_check?: boolean;
}
