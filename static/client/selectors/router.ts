import {createSelector} from 'reselect';

import {CommonStore} from '__utils/infrastructure/store';

export const pathnameSelector = createSelector(
    (state: CommonStore) => state?.router?.location?.pathname,
    (pathname: string) => pathname,
);
