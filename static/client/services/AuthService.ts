import {notification} from 'antd';

import {bound as commonActions} from '__actions';
import {apiInstance} from '__api/AuthAPI';
import {
    DEFAULT_RESPONSE_CATCH_MESSAGE,
    DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE,
} from '__utils/consts';
import {AuthRequest} from '__types/entity/auth/types';

import {localStorageService} from './LocalStorageService';

class AuthService {
    public handleRequest = async (data: AuthRequest) => {
        try {
            const response = await apiInstance.request(data);

            if (response.message.status === 'success') {
                const {
                    data: {
                        token,
                        user: {
                            role,
                            username,
                            id,
                        }
                    }
                } = response;

                localStorageService.setItems([
                    {
                        key: 'token',
                        value: token,
                    },
                    {
                        key: 'user',
                        value: {
                            username,
                            id,
                        }
                    },
                    {
                        key: 'role',
                        value: role,
                    }
                ]);

                notification.success({message: `Добро пожаловать, ${response.data.user.username}`});

                commonActions.router.push('/');
            } else if (response.message.status === 'error') {
                return response.message.error;
            } else {
                notification.error({message: DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE});
            }
        } catch (error) {
            notification.error({message: DEFAULT_RESPONSE_CATCH_MESSAGE});
        }
    }

    public logout = () => {
        localStorageService.clear();

        commonActions.router.push('/auth');
    }

    public checkUser = () => {
        const token = localStorageService.getItem('token');

        if (!token) {
            return commonActions.router.push('/auth');
        }

        return token;
    }
}

export const authService = new AuthService();
