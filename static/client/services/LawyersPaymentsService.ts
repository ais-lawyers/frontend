import {notification} from 'antd';
import moment from 'moment';

import {
    DEFAULT_RESPONSE_CATCH_MESSAGE,
    DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE,
} from '__utils/consts';
import {CreatePaymentsRequest} from '__types/entity/payments/types';
import {IDType} from '__types/entity/api';

import {apiInstance} from '../api/PaymentsAPI';

class LawyersPaymentsService {
    public create = async (data: CreatePaymentsRequest) => {
        try {
            const date = moment(data.date).format('YYYY-MM-DD');
            data.date = date;

            const response = await apiInstance.create(data);

            if (response.message.status === 'success') {
                notification.success({message: `Запись об оплате адвокату ${data.lawyer} добавлена`});
            } else if (response.message.status === 'error') {
                notification.error({message: response.message.error});

                return response.message.error;
            }else {
                notification.error({message: DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE});
            }
        } catch (error) {
            notification.error({message: DEFAULT_RESPONSE_CATCH_MESSAGE});
        }
    }

    public update = async (data: CreatePaymentsRequest, id: IDType) => {
        try {
            const date = moment(data.date).format('YYYY-MM-DD');
            data.date = date;

            const response = await apiInstance.update(data, id);

            if (response.message.status === 'success') {
                notification.success({message: `Запись об оплате адвокату ${data.lawyer} изменена`});
            } else if (response.message.status === 'error') {
                notification.error({message: response.message.error});

                return response.message.error;
            }else {
                notification.error({message: DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE});
            }
        } catch (error) {
            notification.error({message: DEFAULT_RESPONSE_CATCH_MESSAGE});
        }
    }
}

export const lawyersPaymentsService = new LawyersPaymentsService();
