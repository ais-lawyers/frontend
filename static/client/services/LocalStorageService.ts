/* eslint-disable no-unused-vars */
import {LocalStorageItems} from '__types/localStorage';

export class LocalStorageService {
    public setItem = (key: string, value: any) => {
        const stringifiedValue = JSON.stringify(value);
        localStorage.setItem(key, stringifiedValue);
    };

    public setItems = (items: LocalStorageItems) => {
        items.forEach(({key, value}) => {
            this.setItem(key, value);
        });
    };

    public getItem = (key: string) => {
        try {
            return JSON.parse(localStorage.getItem(key));
        } catch (error) {
            return;
        }
    };

    public getItems = () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const {length, getItem, removeItem, clear, key, setItem, ...localStorageData} = localStorage;

        const localStorageItems = [];

        for (const key in localStorageData) {
            if (localStorageData.hasOwnProperty(key)) {
                const value = JSON.parse(localStorageData[key]);

                localStorageItems.push({key, value});
            }
        }

        return localStorageItems;
    };

    public removeItem = (key: string) => localStorage.removeItem(key);

    public removeItems = (items: LocalStorageItems) => {
        items.forEach(({key})=> {
            this.removeItem(key);
        });
    }

    public clear = () => localStorage.clear();

    public length = () => localStorage.length;
}

export const localStorageService = new LocalStorageService();
