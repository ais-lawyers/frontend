import {notification} from 'antd';

import {apiInstance} from '__api/SearchAPI';
import {DEFAULT_RESPONSE_CATCH_MESSAGE} from '__utils/consts';

import {AutocompleteType} from '__types/autocomplete';


class SearchService {
    public search = async (searchString: string, type: AutocompleteType) => {
        try {
            const response = await apiInstance.search(searchString, type);

            if (response.message.status === 'success') {
                return response.data.data.map(item => ({value: item}));
            } else {
                return [];
            }
        } catch (error) {
            notification.error({message: DEFAULT_RESPONSE_CATCH_MESSAGE});
        }
    }
}

export const searchService = new SearchService();
