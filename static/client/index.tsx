import {ConfigProvider} from 'antd';
import ruRU from 'antd/es/locale/ru_RU';
import {ConnectedRouter} from 'connected-react-router';
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import Core from './containers/Core';
import store, {history} from './utils/infrastructure/store';

import './sass/main.scss';

ReactDOM.render(
    (
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <ConfigProvider locale={ruRU}>
                    <Core/>
                </ConfigProvider>
            </ConnectedRouter>
        </Provider>
    ),
    document.getElementById('root'),
);
