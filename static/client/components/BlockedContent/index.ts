import {memo} from 'react';

import BlockedContent from './BlockedContent';

export default memo(BlockedContent);
