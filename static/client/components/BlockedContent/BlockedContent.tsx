import React from 'react';
import {Result} from 'antd';

const BlockedContent = () => {
    return (
        <Result
            status="error"
            title="Раздел доступен только с правами администратора"
            subTitle="Обратитесь к администратору за необходимыми функциями"
        />
    );
}

export default BlockedContent;
