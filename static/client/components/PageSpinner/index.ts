import {memo} from 'react';

import PageSpinner from './PageSpinner';

export default memo(PageSpinner);
