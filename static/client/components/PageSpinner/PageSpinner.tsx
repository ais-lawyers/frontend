import {Spin} from 'antd';
import React, {FC} from 'react';

import {b} from './PageSpinner.scss';

const PageSpinner: FC = () => {
    return (
        <section className={b()}>
            <Spin/>
        </section>
    )
}

export default PageSpinner;
