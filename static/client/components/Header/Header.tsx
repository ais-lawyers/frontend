import {Typography, Tooltip} from 'antd';
import {UserOutlined, LogoutOutlined} from '@ant-design/icons';
import React from 'react';

import {makeRolesTags} from '__utils/infrastructure/mapRoles';
import {localStorageService} from '__services/LocalStorageService';
import {authService} from '__services/AuthService';

import RouteLink from '__components/RouteLink';

import {b} from './Header.scss';

const Header = () => {
    const username = localStorageService.getItem('user').username;
    const role = localStorageService.getItem('role').role;

    return (
        <header className={b()}>
            <section className={b('logo')} style={{display: 'flex'}}>
                <RouteLink url='/' theme='unstyled'>
                    <img src="/dist/images/logo.png" alt="Логотип АИС Адвокаты"/>
                    <Typography.Title level={3}>АИС "Адвокаты"</Typography.Title>
                </RouteLink>
            </section>

            <section className={b('userInfo')}>
                <UserOutlined/>&nbsp;
                <span>Имя пользователя: <b>{username}</b></span>&nbsp;
                <span>Роль: <b>{makeRolesTags(role)}</b></span>&nbsp;

                <Tooltip title="Выход">
                    <LogoutOutlined className={b('logout')} onClick={() => authService.logout()}/>
                </Tooltip>
            </section>
        </header>
    )
}

export default Header;
