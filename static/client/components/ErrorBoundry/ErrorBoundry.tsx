import {Button, Result} from 'antd';
import React from 'react';

import config from '__config/config';

import {Props, State} from './types';

import {b} from './ErrorBoundry.scss';

const handleClickReturn = (event: React.MouseEvent) => {
    event.preventDefault();
    window.location.assign('/widgets');
};

const extra = (
    <Button type="primary" onClick={handleClickReturn}>
        Back Home
    </Button>
);

export default class ErrorBoundary extends React.PureComponent<Props, State> {
    public state: State = {
        error: null,
        errorInfo: null,
    };

    public componentDidCatch(error, errorInfo) {
        this.setState({error, errorInfo});
    }

    public render() {
        const {state} = this;
        const {children} = this.props;

        if (state.errorInfo) {
            if (config.__DEV__) {
                return (
                    <React.Fragment>
                        <h2>Something went wrong.</h2>
                        <details className={b('details')}>
                            {state.error?.toString()}
                            <br/>
                            {state.errorInfo.componentStack}
                        </details>
                    </React.Fragment>
                );
            }

            return (
                <Result
                    status="500"
                    title="Sorry, the application crashed"
                    subTitle="We are already fixing the problem."
                    extra={extra}
                />
            );
        }

        return children;
    }
}
