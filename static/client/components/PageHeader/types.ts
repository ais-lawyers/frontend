import {FC} from 'react';

export interface OwnProps {
    title: JSX.Element;
    subTitle: string
}

export type Props = FC<OwnProps>;
