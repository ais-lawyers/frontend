import {PageHeader as VendorPageHeader} from 'antd';
import React from 'react';

import {Props} from './types';

import {b} from './PageHeader.scss';

const PageHeader: Props = ({title, subTitle}) => {
    return (
        <VendorPageHeader
            title={title}
            subTitle={subTitle}
            className={b()}
        />
    );
}

export default PageHeader;
