import {memo} from 'react';

import PageHeader from './PageHeader';

export default memo(PageHeader);
