import {Button as VendorButton, Popconfirm} from 'antd';
import React, {PureComponent} from 'react';

import {Props, State} from './types';

export default class Button extends PureComponent<Props, State> {
    public state = {
        isPending: false,
    };

    public render() {
        const {children, ...props} = this.props;
        const {loading, popconfirmoptions: popconfirmOptions} = props;
        const {isPending} = this.state;

        if (popconfirmOptions && popconfirmOptions.show) {
            const {title, okText, cancelText, okType} = popconfirmOptions;
            return (
                <React.Fragment>
                    <Popconfirm
                        title={title}
                        okText={okText}
                        cancelText={cancelText}
                        okType={okType}
                        onConfirm={this.handleConfirm}
                        onCancel={evt => evt.stopPropagation()}
                    >
                        <VendorButton
                            {...props}
                            onClick={this.handleClick}
                            loading={loading || isPending}
                        >
                            {children}
                        </VendorButton>
                    </Popconfirm>
                </React.Fragment>
            )
        }
        return (
            <VendorButton
                {...props}
                onClick={this.handleClick}
                loading={loading || isPending}
            >
                {children}
            </VendorButton>
        );
    }

    private handleConfirm = (event: React.MouseEvent<HTMLElement>) => {
        const {popconfirmoptions: {onConfirm}} = this.props;

        if (onConfirm) {
            new Promise(resolve => {
                this.setState({isPending: true});
                return resolve(onConfirm(event));
            })
                .then(this.stopPending)
                .catch(this.stopPending);
        }
    };

    private handleClick = (event: React.MouseEvent<HTMLElement>) => {
        const {onClick} = this.props;

        if (onClick) {
            new Promise(resolve => {
                this.setState({isPending: true});
                return resolve(onClick(event));
            })
                .then(this.stopPending)
                .catch(this.stopPending);
        }
    };

    private stopPending = () => {
        this.setState({isPending: false});
    }
}
