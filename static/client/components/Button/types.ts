import {ReactNode} from 'react';

import {ButtonProps} from 'antd/lib/button';
import {LegacyButtonType} from 'antd/lib/button/button';

export interface PopconfirmOptions {
    popconfirmoptions?: {
        title?: JSX.Element;
        okText?: ReactNode;
        cancelText?: ReactNode;
        okType?: LegacyButtonType;
        onConfirm?: (e?: React.MouseEvent<HTMLElement, MouseEvent>) => void;
        show: boolean;
    };
}

export type OwnProps = ButtonProps & PopconfirmOptions;

export type Props = OwnProps;

export interface State {
    isPending: boolean;
}
