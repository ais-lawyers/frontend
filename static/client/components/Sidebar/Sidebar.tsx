import {Menu} from 'antd';
import React from 'react';
import {useSelector} from 'react-redux';

import {bound as commonActions} from '__actions';
import {pathnameSelector} from '__selectors/router';
import {localStorageService} from '__services/LocalStorageService';

import {makeMenuItems} from './menuItems';

import {b} from './Sidebar.scss';

const onMenuItemClick = (url: string) => () => {
    commonActions.router.push(url);
}

const Sidebar = () => {
    const path = useSelector(pathnameSelector);

    const urlArr = path.split('/');

    const menuItems = makeMenuItems();

    return (
        <div className={b()}>
            <Menu
                theme="light"
                className={b('menu')}
                mode="inline"
                selectedKeys={[`/${urlArr[1]}`]}
            >
                {menuItems.map(({text, url, icon, key, disabled = false, title}, id) => (
                    <Menu.Item
                        key={key}
                        icon={icon}
                        onClick={onMenuItemClick(url)}
                        disabled={disabled}
                        title={title}
                    >
                        {text}
                    </Menu.Item>
                ))}
            </Menu>
        </div>
    )
}

export default Sidebar;
