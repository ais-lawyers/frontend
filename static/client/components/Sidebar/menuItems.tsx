import React from 'react';
import {
    DollarOutlined,
    HomeOutlined,
    UserSwitchOutlined,
    SearchOutlined,
    RobotOutlined,
} from '@ant-design/icons';


import {isAdmin} from '__utils/isAdmin';

import {MenuItems} from './types';

export const makeMenuItems = (): MenuItems => ([
    {
        title: 'Главная',
        text: 'Главная',
        url: '/',
        icon: <HomeOutlined/>,
        key: '/',
    },
    {
        title: isAdmin() ? 'Пользователи' : 'Редактирование пользователей доступно администратору',
        text: 'Пользователи',
        url: '/users',
        icon: <UserSwitchOutlined/>,
        key: '/users',
        disabled: !isAdmin(),
    },
    {
        title: 'Оплата адвокату',
        text: 'Оплата адвокату',
        url: '/lawyer-payments',
        icon: <DollarOutlined/>,
        key: '/lawyer-payments',
    },
    {
        title: 'Поиск и отчеты',
        text: 'Поиск и отчеты',
        url: `/search/1/""/filter_lawyer/filter_mounth/""`,
        icon: <SearchOutlined/>,
        key: '/search',
    },
    {
        title: 'Платное DLC',
        text: 'Автозаполнение из скана',
        url: '/autho',
        icon: <RobotOutlined/>,
        key: '/auto',
        disabled: true,
    },
]);
