export interface MenuItem {
    text: string;
    url: string;
    icon: JSX.Element;
    key: string;
    disabled?: boolean;
    title: string;
}

export type MenuItems = MenuItem[];
