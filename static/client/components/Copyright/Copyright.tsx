import React from 'react';
import {useSelector} from 'react-redux';
import {MailOutlined} from '@ant-design/icons';

import {pathnameSelector} from '__selectors/router';

import {Props} from './types'

import {b} from './Copyright.scss';

const Copyright: Props = () => {
    const path = useSelector(pathnameSelector);

    return (
        <section className={b('', {main: path !== '/auth'})}>
            <span>Copyright© 2020</span>
            <div>
                <span>Frontend, CI/CD, Administration: Поляков Д.А.</span> &nbsp;
                <a href="mailTo: inbox@polyacovda.ru" target="_blank">
                    <MailOutlined/> &nbsp;inbox@polyacovda.ru
                </a>
            </div>
            <div>
                <span>Backend, LocalStackOverflow: Зайцев Н.Н.</span> &nbsp;
                <a href="mailTo: inbox@polyacovda.ru" target="_blank">
                    <MailOutlined/> &nbsp;zaycev.nick@gmail.com
                </a>
            </div>
        </section>
    )
}

export default Copyright;
