import {memo} from 'react';

import Copyright from './Copyright';

export default memo(Copyright);
