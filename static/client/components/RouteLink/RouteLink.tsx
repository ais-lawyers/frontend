import cn from 'classnames';
import React, {PureComponent} from 'react';
import {Link} from 'react-router-dom';

import {Props} from './types';

import {b} from './RouteLink.scss';

export default class RouteLink extends PureComponent<Props> {
    public static readonly defaultProps: Partial<Props> = {
        theme: 'link',
    };

    public render() {
        const {children, style, theme, url} = this.props;

        return (
            <Link
                to={url}
                className={cn(b(), b('theme', {[theme]: true}))}
                style={style}
            >
                { children }
            </Link>
        );
    }
}
