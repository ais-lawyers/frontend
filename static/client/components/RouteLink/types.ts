import {CSSProperties} from 'react';

export type Theme = 'link' | 'black' | 'unstyled';

export interface OwnProps {
    url?: string;
    style?: CSSProperties;
    theme?: Theme;
    isNewTab?: boolean;
}

export type Props = OwnProps;
