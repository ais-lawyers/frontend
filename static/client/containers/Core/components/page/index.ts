import {compose} from 'redux';
import {connect} from 'react-redux';
import {ComponentType} from 'react';

import {pathnameSelector} from '__selectors/router';
import {CommonStore} from '__utils/infrastructure/store';
import preload from '__utils/hocs/preload';

import {onLoad} from './preloader';
import Page from './Page';
import {StateProps} from './types';

const mapStateToProps = (state: CommonStore): StateProps => ({
    pathname: pathnameSelector(state),
});

export default compose<ComponentType>(
    connect(mapStateToProps),
    preload({onLoad}),
)(Page);
