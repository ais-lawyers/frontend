import React from 'react';
import {Switch} from 'react-router';

import {getRoutes} from '__utils/routes/makeRoutes';
import {localStorageService} from '__services/LocalStorageService';

import Copyright from '__components/Copyright';
import Header from '__components/Header';
import Sidebar from '__components/Sidebar';

import {Props} from './types';

import {b} from './Page.scss';

import '__containers/Auth';
import '__containers/Home';
import '__containers/Users';
import '__containers/LawyerPayments';
import '__containers/Search';
import '__containers/NotFound';

export default class Page extends React.PureComponent<Props> {
    public render() {
        const isUserLoggednIn = !!localStorageService.getItem('token');

        return (
            <section className={b('main')}>
                {isUserLoggednIn && <Header/>}

                <div className={b('wrapper')}>
                    {isUserLoggednIn && <Sidebar/>}

                    <div className={b('content', {isAuthorized: !isUserLoggednIn})}>
                        <Switch children={getRoutes()}/>
                        <Copyright/>
                    </div>
                </div>
            </section>
        );
    }
}
