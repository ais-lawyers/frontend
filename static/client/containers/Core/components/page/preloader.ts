import {authService} from '__services/AuthService';

import {Props} from './types';

export function onLoad ({pathname}: Props) {
    if (pathname !== '/auth'){
        authService.checkUser();
    }
}
