export interface StateProps {
    pathname: string;
}

export type Props = StateProps;
