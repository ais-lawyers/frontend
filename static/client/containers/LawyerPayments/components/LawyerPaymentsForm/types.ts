import {CreatePaymentsRequest} from "__types/entity/payments/types";
import {AutocompleteItems, AutocompleteStatuses} from "__types/autocomplete";

export type Model  = CreatePaymentsRequest;

export interface State {
    error: string;
    lawyers: AutocompleteItems;
    defendants: AutocompleteItems;
    judges: AutocompleteItems;
    lawyersEducations: AutocompleteItems;
    lawyersStatus: AutocompleteStatuses;
    judgesStatus: AutocompleteStatuses;
    lawyersEducationsStatus: AutocompleteStatuses;
    defendantsStatus: AutocompleteStatuses;
    [key: string]: any; //нухз аа блядь все пропало
}

export type Props = {};
