import {
    Form,
    DatePicker,
    InputNumber,
    Select,
    Checkbox,
    Col,
    Row,
    Alert,
    AutoComplete,
    Empty,
    Tooltip,
} from 'antd';
import {FormInstance} from 'antd/lib/form';
import isEmpty from 'lodash/isEmpty';
import debounce from 'lodash/debounce';
import {PlusOutlined} from '@ant-design/icons';
import React, {PureComponent} from 'react';

import makeModel from '__utils/infrastructure/makeModel';
import {isAdmin} from '__utils/isAdmin';
import {CASE_TYPE} from '__types/cases';
import {searchService} from '__services/SearchService';
import {AutocompleteType} from '__types/autocomplete';

import Button from '__components/Button';

import {lawyersPaymentsService} from '../../../../services/LawyersPaymentsService';
import {CreatePaymentsRequest} from '../../../../types/entity/payments/types';
import {Model, State, Props} from './types';

import {b} from './LawyerPaymentsForm.scss';

export default class LawyerPaymentsForm extends PureComponent<Props, State> {
    public state: State = {
        error: '',
        lawyers: [],
        judges: [],
        defendants: [],
        lawyersEducations: [],
        lawyersStatus: '',
        judgesStatus: '',
        defendantsStatus: '',
        lawyersEducationsStatus: '',
    }

    public formRef = React.createRef<FormInstance>();

    public render() {
        const {
            error,
            lawyers,
            judges,
            defendants,
            lawyersEducations,
            lawyersStatus,
            judgesStatus,
            defendantsStatus,
            lawyersEducationsStatus,
    } = this.state;

        return (
            <Form
                className={b()}
                layout="vertical"
                size="large"
                ref={this.formRef}
            >
                <Row gutter={[12, 0]}>
                    <Col span={12}>
                        <Form.Item
                            label="ФИО адвоката"
                            name={makeModel<Model>(m => m.lawyer)}
                            rules={[
                                {required: true, message: 'Введите ФИО адвоката'},
                                {max: 16, message: 'Максимум 16 символов'}
                            ]}
                            hasFeedback
                            validateStatus={lawyersStatus}
                        >
                            <AutoComplete
                                options={lawyers}
                                placeholder="Борис Юрьевич Александров"
                                onSearch={debounce((value) => this.onSearch('lawyers')(value), 300)}
                                notFoundContent={<Empty/>}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item
                            label="Адвокатское образование"
                            name={makeModel<Model>(m => m.lawyer_education)}
                            rules={[
                                {required: true, message: 'Введите адвокатское образование'},
                                {max: 16, message: 'Максимум 16 символов'}
                            ]}
                            hasFeedback
                            validateStatus={lawyersEducationsStatus}
                        >
                            <AutoComplete
                                placeholder="Образование им. П.П. Кащенко"
                                onSearch={debounce((value) => this.onSearch('lawyers-educations')(value), 300)}
                                notFoundContent={<Empty/>}
                                options={lawyersEducations}
                            />
                        </Form.Item>
                    </Col>
                </Row>

                <Row gutter={[12, 0]}>
                <Col span={12}>
                        <Form.Item
                            label="ФИО судьи"
                            name={makeModel<Model>(m => m.judge)}
                            rules={[
                                {required: true, message: 'Введите ФИО судьи'},
                                {max: 16, message: 'Максимум 16 символов'}
                            ]}
                            hasFeedback
                            validateStatus={judgesStatus}
                        >
                            <AutoComplete
                                placeholder="Хахалева Елена Владимировна"
                                onSearch={debounce((value) => this.onSearch('judges')(value), 300)}
                                notFoundContent={<Empty/>}
                                options={judges}
                            />
                        </Form.Item>
                    </Col>

                    <Col span={12}>
                        <Form.Item
                            label="Дата постановления"
                            name={makeModel<Model>(m => m.date)}
                            rules={[
                                {required: true, message: 'Выберите дату постановления'},
                            ]}
                        >
                            <DatePicker className={b('datePickerInput')} format={'DD.MM.YYYY'}/>
                        </Form.Item>
                    </Col>
                </Row>

                <Row gutter={[12, 0]}>
                    <Col span={12}>
                        <Form.Item
                            label="ФИО подсудимого"
                            name={makeModel<Model>(m => m.defendant)}
                            rules={[
                                {required: true, message: 'Введите имя подсудимого'},
                                {max: 16, message: 'Максимум 16 символов'}
                            ]}
                            hasFeedback
                            validateStatus={defendantsStatus}
                        >
                            <AutoComplete
                                placeholder="Владимир Рудольфович Соловьев"
                                onSearch={debounce((value) => this.onSearch('defendants')(value), 300)}
                                notFoundContent={<Empty/>}
                                options={defendants}
                            />
                        </Form.Item>
                    </Col>

                    <Col span={12}>
                        <Form.Item
                            label="Cумма &#8381;"
                            name={makeModel<Model>(m => m.cash)}
                            rules={[
                                {required: true, message: 'Введите сумму оплаты в рублях'},
                            ]}
                        >
                            <InputNumber
                                placeholder="500"
                                className={b('cashInput')}
                                min={1}
                            />
                        </Form.Item>
                    </Col>
                </Row>

                <Row justify="center">
                    <Col span={12}>
                        <Form.Item
                            label="Тип дела"
                            name={makeModel<Model>(m => m.type_id)}
                            rules={[
                                {required: true, message: 'Выберите тип дела'},
                            ]}
                        >
                            <Select
                                placeholder="Выбирите тип дела"
                            >
                                {CASE_TYPE.map(({id, title}) => (
                                    <Select.Option key={id + title} value={id}>
                                        {title}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>

                <Row justify="center">
                    <Form.Item
                        name={makeModel<Model>(m => m.no_check)}
                        valuePropName="checked"
                    >
                        <Checkbox disabled={!isAdmin()}>
                            <Tooltip title={isAdmin() ? 'Убирает проверку на дубликат' : 'Доступно администратору'}>
                                <span>Без проверки на дублирование</span>
                            </Tooltip>
                        </Checkbox>
                    </Form.Item>

                    <Button type="primary" htmlType="submit" icon={<PlusOutlined/>} onClick={this.onCreatePayments}>Добавить</Button>
                </Row>

                <Row>
                    <Col span={24}>
                        {error && (
                            <Alert
                                message="Ошибка"
                                description={error}
                                type="error"
                                showIcon
                                closable
                            />
                        )}
                    </Col>
                </Row>
            </Form>
        );
    }

    private onCreatePayments = async () => {
        const form = this.formRef.current;

        await form.validateFields()
            .then(async (values) => {
                const error = await lawyersPaymentsService.create(values as CreatePaymentsRequest);
                console.log('values', values)
                if (!error) {
                    form.resetFields();
                    this.resetState();
                }

                error && this.setState({error});
            });
    }

    private onSearch = (type: AutocompleteType) => async (searchString: string) => {
        if (searchString.length >= 2) {
            let convertedType: any = type;

            if (type === 'lawyers-educations') {
                convertedType = 'lawyersEducations';
            }

            this.setState({[`${convertedType}Status`]: 'validating'});

            const response = await searchService.search(searchString, type);

            this.setState({[convertedType]: response});

            if (isEmpty(response)) {
                return this.setState({[`${convertedType}Status`]: ''});
            }

            this.setState({[`${convertedType}Status`]: 'success'});
        }
    }

    private resetState = () => {
        this.setState({
            error: '',
            lawyers: [],
            judges: [],
            defendants: [],
            lawyersEducations: [],
            lawyersStatus: '',
            judgesStatus: '',
            defendantsStatus: '',
            lawyersEducationsStatus: '',
        })
    }
}
