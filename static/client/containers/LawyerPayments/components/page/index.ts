import {memo} from 'react';

import asPage from '__utils/hocs/asPage';

import Page from './Page';

export default asPage({
    name: 'АИС Адвокаты | Оплата адвокату',
    title: 'АИС Адвокаты | Оплата адвокату',
})(memo(Page));
