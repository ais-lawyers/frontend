import React from 'react';

import PageHeader from '__components/PageHeader';

import LawyerPaymentsForm from '../LawyerPaymentsForm';

import {b} from './Page.scss';

const Page = () => {
    return (
        <div className={b()}>
            <PageHeader title={<span>Оплата адвокату</span>} subTitle="здесь заносятся оплаты адвокатам"/>

            <div className={b('content')}>
                <LawyerPaymentsForm/>
            </div>
        </div>
    );
}

export default Page;
