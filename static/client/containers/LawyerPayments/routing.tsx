import React from 'react';
import Loadable from 'react-loadable';

import AppRoute from '__components/AppRoute';
import PageSpinner from '__components/PageSpinner';

import {ROOT_PATH} from './consts';

const AsyncPage = Loadable({
    loader: () => import(/* webpackChunkName: "Lawyer payments" */ './components/page'),
    loading: () => <PageSpinner/>,
} as Loadable.Options<unknown, never>);

export default (
    <React.Fragment>
        <AppRoute exact path={ROOT_PATH} component={AsyncPage}/>
    </React.Fragment>
);
