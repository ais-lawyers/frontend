import { authService } from "__services/AuthService";

import {bound as commonActions} from '__actions';

export function onLoad () {
    const isAuthorized = !!authService.checkUser();

    if (isAuthorized) {
        commonActions.router.push('/');
    }
}
