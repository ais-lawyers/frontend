import React from 'react';

import AuthForm from '../auth-form';

import {b} from './Page.scss';

const Page = () => {
    return (
        <section className={b()}>
            <AuthForm/>
        </section>
    )
}

export default Page;
