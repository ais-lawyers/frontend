import preload from '__utils/hocs/preload';

import Page from './Page';
import {onLoad} from './preloader';

export default preload({onLoad})(Page);
