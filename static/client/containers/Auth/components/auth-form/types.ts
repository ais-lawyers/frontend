import {AuthRequest} from '__types/entity/auth/types';

export type Model = AuthRequest

export interface State {
    error: string;
    loading: boolean;
}

export type Props = {};
