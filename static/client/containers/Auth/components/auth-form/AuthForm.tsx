import {Alert, Form, Input, Typography, Tooltip, Button} from 'antd';
import {
    UserOutlined,
    LockOutlined,
    LoginOutlined,
} from '@ant-design/icons';
import {Store} from 'antd/lib/form/interface';
import React, {Component} from 'react';

import makeModel from '__utils/infrastructure/makeModel';
import {authService} from '__services/AuthService';

import {Props, Model, State} from './types';

import {b} from './AuthForm.scss';

export default class AuthForm extends Component<Props, State> {
    public state : State= {
        error: '',
        loading: false,
    }

    public render() {
        const {error, loading} = this.state;

        return (
            <Form
                name="auth"
                onFinish={this.onSubmitAuth}
                className={b()}
            >
                <Typography.Title className={b('title')}>
                    АИС "Адвокаты"
                    <img src="/dist/images/logo.png" alt="Логотип АИС Адвокаты"/>
                </Typography.Title>

                <Form.Item
                    name={makeModel<Model>(m => m.username)}
                    rules={[{required: true, message: 'Введите имя пользователя'}]}
                >
                    <Input
                        prefix={
                            <Tooltip title="Введите логин">
                                <UserOutlined className="site-form-item-icon" />
                            </Tooltip>
                        }
                        placeholder="Б.Ю.Александров"
                        disabled={loading}
                    />
                </Form.Item>

                <Form.Item
                    name={makeModel<Model>(m => m.password)}
                    rules={[{required: true, message: 'Введите пароль'}]}
                >
                    <Input.Password
                        prefix={
                            <Tooltip title="Введите пароль">
                                <LockOutlined className="site-form-item-icon" />
                            </Tooltip>
                        }
                        placeholder="Пароль"
                        disabled={loading}
                    />
                </Form.Item>

                <Button
                    type="primary"
                    htmlType="submit"
                    icon={<LoginOutlined/>}
                    className={b('submit')}
                    loading={loading}
                >
                    Войти
                </Button>

                {error && (
                    <Alert
                        message="Ошибка"
                        description={error}
                        type="error"
                        showIcon
                    />
                )}
            </Form>
        );
    }

    private onSubmitAuth = async (data: Store) => {
        this.setState({loading: true});

        authService.handleRequest(data as Model)
            .then(error => this.setState({error, loading: false}))
    }
}
