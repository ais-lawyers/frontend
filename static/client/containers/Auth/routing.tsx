import React from 'react';
import Loadable from 'react-loadable';

import AppRoute from '__components/AppRoute';
import PageSpinner from '__components/PageSpinner';

import {AUTH_PATH} from './consts';

const AsyncPage = Loadable({
    loader: () => import(/* webpackChunkName: "Auth" */ './components/page'),
    loading: () => <PageSpinner/>,
} as Loadable.Options<unknown, never>);

export default (
    <React.Fragment>
        <AppRoute exact path={AUTH_PATH} component={AsyncPage}/>
    </React.Fragment>
);
