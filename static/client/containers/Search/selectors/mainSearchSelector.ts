import {createSelector} from 'reselect';

import {checkPending} from '__utils/infrastructure/reducers/flow';

import {BundleState, MainSearchRequestResponse} from '../types';

export const mainSearchSelector = createSelector(
    (state: BundleState) => state.search?.search.item?.data,
    (data: MainSearchRequestResponse) => data,
);

export const mainSearchIsPendingSelector = createSelector(
    (state: BundleState) => state.search?.search.item?.status,
    checkPending,
);

export const mainSearchItemSelector = (paymentId: string) => createSelector(
    mainSearchSelector,
    (result: MainSearchRequestResponse) => result?.data.filter(({id}) => id === +paymentId)[0],
);
