import {createSelector} from 'reselect';

import {BundleState, DateFilterTypes, SearchType, LatestSearch} from '../types';

export const dateFilterSelector = createSelector(
    (state: BundleState) => state.search?.searchForm.dateFilter,
    (dateFilter: DateFilterTypes) => dateFilter,
);

export const searchTypeSelector = createSelector(
    (state: BundleState) => state.search?.searchForm.searchType,
    (searchType: SearchType) => searchType,
);

export const latestSearchSelector = createSelector(
    (state: BundleState) => state.search?.searchForm.latestSearch,
    (searchType: LatestSearch) => searchType,
);
