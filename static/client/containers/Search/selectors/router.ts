import {RouteProps} from '../types';

export const getPaymentsPage = (routeProps: RouteProps) =>
    routeProps.match.params?.page || '1';

export const getPaymentId = (routeProps: RouteProps) =>
    routeProps.match.params?.paymentId;

export const getSearchField = (routeProps: RouteProps) => 
    routeProps.match.params?.searchField

export const getSearchType = (routeProps: RouteProps) =>
    routeProps.match.params?.searchType

export const getDateType = (routeProps: RouteProps) =>
    routeProps.match.params?.dateType

export const getDate = (routeProps: RouteProps) =>
    routeProps.match.params?.date
