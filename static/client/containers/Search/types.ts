import {RouteComponentProps} from 'react-router';

import {ExtendedState} from '__utils/infrastructure/store';

import {SearchReducers} from './reducers';
import { IDType } from '__types/entity/api';

export type DateFilterTypes = 'filter_date' | 'filter_period' | 'filter_mounth' | 'filter_all';

export type SearchType = 'filter_lawyer' | 'filter_defendant' | 'filter_decision_date' | 'filter_all';


export type BundleState = ExtendedState<{
    search: SearchReducers;
}>;

export interface LatestSearch {
    dateFilter: DateFilterTypes;
    searchType: SearchType;
    searchPhrase: string;
    date: string;
}

export interface MainSearch {
    filter_lawyer?: string;
    filter_defendant?: string;
    filter_all?: string;
    filter_decision_date?: string;
    filter_creation_date?: string;
    page?: IDType;
}

export interface SearchItem {
    cash: number;
    date: string;
    defendant: string;
    id: number;
    lawyer: string;
    lawyer_education: string;
    judge: string;
    type: 'Административное' | 'Гражданское' | 'Уголовное';
    created_at: string;
}

export interface MainSearchRequestResponse {
    data: SearchItem[];
    page: number;
    page_size: number;
    total_items: number;
    total_pages: number;
}

export interface RouterProps {
    page: string;
    paymentId: string;
    searchField: string;
    searchType: SearchType;
    dateType: DateFilterTypes;
    date: string;
}

export type RouteProps = RouteComponentProps<RouterProps>;
