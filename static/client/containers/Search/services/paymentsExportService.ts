import {notification} from 'antd';
import FileSaver from 'file-saver';

import {DEFAULT_RESPONSE_CATCH_MESSAGE} from '__utils/consts';
import {makePeriod, makeDate, makeAllTime, makeMounth} from '__utils/date/makeDates';

import {apiInstance} from '../api/paymentsExportAPI';
import {Model} from '../components/search-form/types';
import {MainSearch} from '../types';

class PaymentsExportService {
    public handleRequest = async ({searchField, searchType, dateType, date}: Model) => {
        try {
            let requestDate = '';
            switch (dateType) {
                case 'filter_period':
                    requestDate = makePeriod(date as string);
                    break;

                case 'filter_date':
                    requestDate = makeDate(date as string);
                    break;

                case 'filter_all':
                    requestDate = makeAllTime();
                    break;

                case 'filter_mounth':
                default:
                    requestDate = makeMounth();
                    break;
            }

            let searchRequestData: MainSearch = {};

            let searchFieldValue = searchField;

            if (searchFieldValue === '""') {
                searchFieldValue = undefined;
            }

            if (searchType === 'filter_decision_date') {
                searchRequestData = {
                    filter_decision_date: requestDate,
                }
            } else {
                searchRequestData = {
                    [searchType]: searchFieldValue,
                    filter_creation_date: requestDate,
                }
            }

            const blob = await apiInstance.request(searchRequestData);

            FileSaver.saveAs(blob);
        } catch (error) {
            console.log('error', error)
            notification.error({message: DEFAULT_RESPONSE_CATCH_MESSAGE});
        }
    }
}

export const paymentsExportService = new PaymentsExportService();
