import {notification} from 'antd';
import isEmpty from 'lodash/isEmpty';

import {bound as commonActions} from '__actions';
import {DEFAULT_RESPONSE_CATCH_MESSAGE, DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE} from '__utils/consts';
import {makePeriod, makeDate, makeAllTime, makeMounth} from '__utils/date/makeDates';
import {IDType} from '__types/entity/api';

import {apiInstance} from '../api/mainSearchAPI';
import {Model} from '../components/search-form/types';
import {searchLoader} from '../loaders/searchLoader';
import {MainSearch, MainSearchRequestResponse, SearchType, DateFilterTypes} from '../types';
import {ROOT_PATH} from '../consts';
import {makePath} from '../components/utils/makePath';

class MainSearchService {
    public handleRequest = async ({searchField, searchType, dateType, date}: Model, page: IDType) => {
        try {
            let requestDate = '';
            switch (dateType) {
                case 'filter_period':
                    requestDate = makePeriod(date as string);
                    break;

                case 'filter_date':
                    requestDate = makeDate(date as string);
                    break;

                case 'filter_all':
                    requestDate = makeAllTime();
                    break;

                case 'filter_mounth':
                default:
                    requestDate = makeMounth();
                    break;
            }

            let searchRequestData: MainSearch = {page};

            let searchFieldValue = searchField;

            if (searchFieldValue === '""') {
                searchFieldValue = undefined;
            }

            if (searchType === 'filter_decision_date') {
                searchRequestData = {
                    filter_decision_date: requestDate,
                    page,
                }
            } else {
                searchRequestData = {
                    [searchType]: searchFieldValue,
                    filter_creation_date: requestDate,
                    page,
                }
            }

            return await searchLoader({...searchRequestData});
        } catch (error) {
            notification.error({message: DEFAULT_RESPONSE_CATCH_MESSAGE});
        }
    }

    public delete = async (paymentId: IDType, page: IDType, searchType: SearchType, dateType: DateFilterTypes, date: string, searchField: string) => {
        try {
            const response = await apiInstance.delete(paymentId);

            if (response.message.status === 'success') {
                const {page: currentPage, data}: MainSearchRequestResponse = await this.handleRequest({searchField, searchType, dateType, date}, page);

                notification.success({message: 'Запись удалена'});

                if (isEmpty(data)) {
                    if (page !== 1) {
                        commonActions.router.push(makePath(currentPage - 1, searchField, searchType, dateType, date));
                    }
                }
            } else if (response.message.status === 'error') {
                notification.error({message: response.message.error});
            }else {
                notification.error({message: DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE});
            }
        } catch (error) {
            notification.error({message: DEFAULT_RESPONSE_CATCH_MESSAGE});
        }
    }
}

export const mainSearchService = new MainSearchService();
