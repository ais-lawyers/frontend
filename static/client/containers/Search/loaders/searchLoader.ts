import {item} from '../reducers/search';
import {entityLoader} from '__utils/infrastructure/reducers/entityLoader';

import {apiInstance} from '../api/mainSearchAPI';

export const searchLoader = entityLoader(apiInstance, {actions: item.actions});
