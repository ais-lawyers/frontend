import React from 'react';
import Loadable from 'react-loadable';

import {Modes} from '__types/form';

import AppRoute from '__components/AppRoute';
import PageSpinner from '__components/PageSpinner';

import {ROOT_PATH} from './consts';

const AsyncPage = Loadable({
    loader: () => import(/* webpackChunkName: "Search" */ './components/page'),
    loading: () => <PageSpinner/>,
} as Loadable.Options<unknown, never>);

export default (
    <React.Fragment>
        <AppRoute exact path={ROOT_PATH} component={AsyncPage}/>

        <AppRoute exact path={`${ROOT_PATH}/:page/:searchField/:searchType/:dateType/:date`} component={AsyncPage}/>

        <AppRoute
            exact
            path={`${ROOT_PATH}/:page/:searchField/:searchType/:dateType/:date/edit/:paymentId`}
            component={AsyncPage}
            componentProps={{
                mode: Modes.Edit
            }}
        />
    </React.Fragment>
);
