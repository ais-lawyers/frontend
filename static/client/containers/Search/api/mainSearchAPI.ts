import {EntityAPI} from 'infrastructure/common';
import {BaseResponse} from 'core';

import {IDType} from '__types/entity/api';
import {paymentsApi} from '__utils/transport';

import {MainSearch, MainSearchRequestResponse} from '../types';

class MainSearchAPI implements EntityAPI {
    public request = (data: MainSearch) =>
        paymentsApi.get<MainSearch, BaseResponse<MainSearchRequestResponse>>('', data)
            .then(({data})=> data);

    public delete = (paymentId: IDType) =>
        paymentsApi.delete<void, BaseResponse<null>>(`/${paymentId}`)
            .then(response => response);
}

export const apiInstance = new MainSearchAPI();
