import {EntityAPI} from 'infrastructure/common';
import {BaseResponse} from 'core';

import {paymentsApi} from '__utils/transport';

import {MainSearch} from '../types';

class PaymentsExportAPI implements EntityAPI {
    public request = (data: MainSearch) =>
        paymentsApi.getBlob<MainSearch, BaseResponse<File>>('/export/', data)
            .then(response => response);
}

export const apiInstance = new PaymentsExportAPI();
