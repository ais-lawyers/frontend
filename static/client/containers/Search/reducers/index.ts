import {combineReducers} from 'redux';

import {BaseEntityReducer} from '__utils/infrastructure/reducers/flow';

import {MainSearchRequestResponse} from '../types';
import searchForm, {actions as searchFormActions, SearchFormReducer} from './searchForm';
import {item as searchItem} from './search';

type SearchReducer = BaseEntityReducer<MainSearchRequestResponse>;

export interface SearchReducers {
    searchForm: SearchFormReducer;
    search: SearchReducer;
}

export const actions = {
    searchFormActions,
    search: searchItem.actions,
};

export default combineReducers<SearchReducers>({
    searchForm,
    search: combineReducers<SearchReducer>({
        item: searchItem.reducer,
    }),
});
