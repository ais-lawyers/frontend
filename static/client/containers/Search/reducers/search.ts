import {generateEntityReducer} from '__utils/infrastructure/reducers/flow';

import {MainSearchRequestResponse} from '../types';


const PREFIX = '@search';

const {item} = generateEntityReducer<MainSearchRequestResponse>(PREFIX);
export {item};
