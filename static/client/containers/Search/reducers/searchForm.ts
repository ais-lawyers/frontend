import {createAction, handleActions} from 'redux-actions';

import {Action} from 'infrastructure/common';

import {DateFilterTypes, SearchType, LatestSearch} from '../types';

type ActionTypes = DateFilterTypes | SearchType | LatestSearch;

export interface SearchFormReducer {
    dateFilter: DateFilterTypes;
    searchType: SearchType;
    latestSearch: LatestSearch;
}

const PREFIX = '@search/search-form';
const defaultState: SearchFormReducer = {
    dateFilter: 'filter_mounth',
    searchType: 'filter_lawyer',
    latestSearch: {
        dateFilter: 'filter_mounth',
        searchType: 'filter_lawyer',
        searchPhrase: '',
        date: '',
    }
};

export const actions = {
    setDateFilter: createAction(`${PREFIX}/DATE_FILTER`),
    setSearchType: createAction(`${PREFIX}/SEARCH_TYPE`),
    setLatestSearch: createAction(`${PREFIX}/LATEST_SEARCH`),
};

export default handleActions<SearchFormReducer, any>({
    [actions.setDateFilter.toString()]: (state: SearchFormReducer, {payload}: Action<DateFilterTypes>): SearchFormReducer => ({
        ...state,
        dateFilter: payload,
    }),
    [actions.setSearchType.toString()]: (state: SearchFormReducer, {payload}: Action<SearchType>): SearchFormReducer => ({
        ...state,
        searchType: payload,
    }),
    [actions.setLatestSearch.toString()]: (state: SearchFormReducer, {payload}: Action<LatestSearch>): SearchFormReducer => ({
        ...state,
        latestSearch: payload,
    }),
}, defaultState);
