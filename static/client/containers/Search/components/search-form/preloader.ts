import {bound as searchActions} from '../../actions';
import {Props} from './types';

export function onLoad({initialSearchType, initialDateType}: Props) {
    searchActions.searchFormActions.setSearchType(initialSearchType);
    searchActions.searchFormActions.setDateFilter(initialDateType);
}
