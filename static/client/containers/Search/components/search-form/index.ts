import {connect} from 'react-redux';
import {compose} from 'redux';
import {withRouter} from 'react-router';
import {ComponentType} from 'react';

import preload from '__utils/hocs/preload';

import {BundleState, RouteProps} from '../../types';
import {dateFilterSelector, searchTypeSelector} from '../../selectors/searchFormSelectors';
import {mainSearchIsPendingSelector} from '../../selectors/mainSearchSelector';
import {getPaymentsPage, getSearchField, getSearchType, getDateType, getDate} from '../../selectors/router';
import SearchForm from './SearchForm';
import {StateProps} from './types';
import {onLoad} from './preloader';

const mapStateToProps = (state: BundleState, routeProps: RouteProps): StateProps => ({
    dateFilter: dateFilterSelector(state),
    searchType: searchTypeSelector(state),
    page: getPaymentsPage(routeProps),
    initialSearchField: getSearchField(routeProps),
    initialSearchType: getSearchType(routeProps),
    initialDateType: getDateType(routeProps),
    initialDate: getDate(routeProps),
    isSearchPending: mainSearchIsPendingSelector(state),
});

export default compose<ComponentType>(
    withRouter,
    connect(mapStateToProps),
    preload({onLoad}),
)(SearchForm);
