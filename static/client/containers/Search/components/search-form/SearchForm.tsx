import {Form, Input, Select, Row, Col, DatePicker, AutoComplete} from 'antd';
import {FormInstance} from 'antd/lib/form';
import {SearchOutlined} from '@ant-design/icons';
import React, {PureComponent} from 'react';
import moment, {Moment} from 'moment';
import {isArray, isEmpty, debounce} from 'lodash';

import {bound as commonActions} from '__actions';
import makeModel from '__utils/infrastructure/makeModel';
import {AutocompleteType} from '__types/autocomplete';
import {searchService} from '__services/SearchService';
import {DEFAULT_DATE_FORMAT} from '__utils/date/defaultDateFormat';

import Button from '__components/Button';

import {DateFilterTypes, SearchType} from '../../types';
import {bound as searchActions} from '../../actions';
import {ROOT_PATH} from '../../consts';
import {makePath} from '../utils/makePath';
import {SELECT_SEARCH_TYPE, SELECT_DATE_TYPE, AUTOCOMPETE_TYPE} from './consts';
import {Props, Model, State} from './types';

import {b} from './SearchForm.scss';

export default class SearchForm extends PureComponent<Props> {
    public formRef = React.createRef<FormInstance>();

    public state: State = {
        autocomplete: [],
        autocompleteStatus: '',
        searchPending: false,
    }

    public render() {
        const {searchType, initialDateType, initialSearchType, isSearchPending} = this.props;
        const {autocomplete, autocompleteStatus, searchPending} = this.state;

        const isDecisionSearch = searchType  === 'filter_decision_date';

        return (
            <Form
                className={b()}
                ref={this.formRef}
                size="large"
            >
                {!isDecisionSearch && (
                    <Row justify="center">
                        <Col span={12}>
                            <Form.Item
                                name={makeModel<Model>(m => m.searchField)}
                                initialValue={this.makeSearchFieldInitialValue()}
                            >
                                <AutoComplete
                                    onSearch={debounce((value) => this.onAutocomplete(searchType)(value), 300)}
                                    className={b('search')}
                                    options={autocomplete}
                                >
                                    <Input.Search
                                        placeholder="Поиск..."
                                        enterButton
                                        onSearch={this.onSearch}
                                        loading={searchPending || isSearchPending}
                                    />
                                </AutoComplete>
                            </Form.Item>
                        </Col>
                    </Row>
                )}

                <Row justify="center">
                    <Col span={6}>
                        <Form.Item
                            name={makeModel<Model>(m => m.searchType)}
                           initialValue={initialSearchType}
                        >
                            <Select
                                className={b('select', {isDecisionSearch})}
                                onChange={this.onSearchTypeChange}
                            >
                                {SELECT_SEARCH_TYPE.map(({value, title}) => (
                                     <Select.Option key={value + title} value={value}>
                                         {title}
                                     </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>

                    <Col span={6}>
                        <Form.Item
                            name={makeModel<Model>(m => m.dateType)}
                            initialValue={initialDateType}
                        >
                            <Select
                                className={b('select', {right: true, isDecisionSearch})}
                                onChange={this.onDateFilterChange}
                            >
                                {SELECT_DATE_TYPE.map(({value, title}) => (
                                     <Select.Option key={value + title} value={value}>
                                         {title}
                                     </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>

                    {isDecisionSearch && (
                        <Col span={1}>
                            <Button
                                onClick={this.handleSubmit('')}
                                icon={<SearchOutlined/>}
                                type="primary"
                                className={b('searchBtn')}
                            />
                        </Col>
                    )}
                </Row>

                {this.renderPicker()}
            </Form>
        );
    }

    private makeSearchFieldInitialValue = () => {
        const {initialSearchField} = this.props;

        if (initialSearchField === '""') {
            return undefined;
        }

        return initialSearchField || undefined;
    }

    private onDateFilterChange = (dateFilter: DateFilterTypes) => {
        searchActions.searchFormActions.setDateFilter(dateFilter);

        const form = this.formRef.current;

        form.resetFields(['datepicker', 'rangepicker']);
    }

    private onSearchTypeChange = (searchType: SearchType) => {
        searchActions.searchFormActions.setSearchType(searchType);
    }

    private renderPicker = () => {
        const {dateFilter} = this.props;

        if (dateFilter === 'filter_date') {
            return (
                <Row justify="center">
                    <Col span={6}>
                        <Form.Item
                            name={makeModel<Model>(m => m.datepicker)}
                            rules={[
                                {required: true, message: 'Выберите дату'},
                            ]}
                            initialValue={this.makePickersDate('date')}
                        >
                            <DatePicker
                                format={DEFAULT_DATE_FORMAT}
                                className={b('datepicker')}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            );
        }

        if (dateFilter === 'filter_period') {
            return (
                <Row justify="center">
                    <Col span={6}>
                        <Form.Item
                            name={makeModel<Model>(m => m.rangepicker)}
                            rules={[
                                {required: true, message: 'Выберите период'},
                            ]}
                            initialValue={this.makePickersDate('period')}
                        >
                            <DatePicker.RangePicker
                                format={DEFAULT_DATE_FORMAT}
                                className={b('datepicker')}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            );
        }

        return null;
    }

    private makePickersDate = (type: 'date' | 'period') => {
        const {initialDate} = this.props;

        const date = initialDate.split(',');

        if (type === 'date') {
            return (isEmpty(date[0]) || date[0] === '""')
                ? undefined
                : moment(date[0]).isValid() ? moment(date[0]) : moment(+date[0]);
        } else if (type === 'period') {
            const firstDate = (isEmpty(date[1]) || date[0] === '""') ? undefined : moment(date[0]).isValid() ? moment(date[0]) : moment(+date[0]);

            const secondDate = (isEmpty(date[1]) || date[0] === '""')
                ? undefined
                : moment(date[1]).isValid() ? moment(date[1]) : moment(+date[1]);

            return [firstDate, secondDate];
        }
    }

    private onSearch = (searchPhrase = '') => {
        this.handleSubmit(searchPhrase)();
    }

    private handleSubmit = (searchPhrase: string) => async () => {
        this.setState({searchPending: true});

        const {dateFilter, searchType} = this.props;

        const form = this.formRef.current;

        try {
            const values = await form.validateFields() as Model;

            let date = form.getFieldValue('date');

            if (isArray(date)) {
                date = `с ${moment(date[0]).format(DEFAULT_DATE_FORMAT)} по ${moment(date[1]).format(DEFAULT_DATE_FORMAT)}`;
            } else {
                if (!isEmpty(date)) {
                    date = moment(date).format(DEFAULT_DATE_FORMAT);
                }
            }

            searchActions.searchFormActions.setLatestSearch({searchPhrase, dateFilter, searchType, date});

            const searchFieldValue = values.searchField ? values.searchField : '""';
            const newDate = values.rangepicker ? values.rangepicker : values.datepicker;

            commonActions.router.push(makePath('1', searchFieldValue, values.searchType, values.dateType, newDate as string));
        } finally {
            this.setState({searchPending: false});
        }
    }

    private onAutocomplete = (type: SearchType) => async (searchString: string) => {
        if (searchString.length >= 2) {
            const autocompleteType: AutocompleteType = AUTOCOMPETE_TYPE[type];

            if (autocompleteType) {
                this.setState({autocompleteStatus: 'validating'});

                const response = await searchService.search(searchString, autocompleteType);

                this.setState({
                    autocomplete: response,
                    autocompleteStatus: '',
                });
            }
        }
    }
}
