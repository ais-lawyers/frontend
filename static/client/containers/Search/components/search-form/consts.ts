export const SELECT_SEARCH_TYPE = [
    {
        title: 'По адвокату',
        value: 'filter_lawyer',
    },
    {
        title: 'По подсудимому',
        value: 'filter_defendant',
    },
    {
        title: 'По дате постановления',
        value: 'filter_decision_date',
    },
    {
        title: 'По всей базе данных',
        value: 'filter_all',
    },
]

export const SELECT_DATE_TYPE = [
    {
        title: 'За этот месяц',
        value: 'filter_mounth',
    },
    {
        title: 'За дату',
        value: 'filter_date',
    },
    {
        title: 'За период',
        value: 'filter_period',
    },
    {
        title: 'За все время',
        value: 'filter_all',
    },
]

export const AUTOCOMPETE_TYPE = {
    filter_lawyer: 'lawyers',
    filter_defendant: 'defendants',
}