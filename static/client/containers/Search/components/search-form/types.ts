import {AutocompleteItems, AutocompleteStatuses} from '__types/autocomplete';

import {DateFilterTypes, SearchType} from '__containers/Search/types';

export interface Model {
    searchField: string;
    datepicker?: string;
    rangepicker?: [string, string];
    searchType: SearchType;
    dateType: DateFilterTypes;
    date?: string | [string, string];
}

export interface StateProps {
    dateFilter: DateFilterTypes;
    searchType: SearchType;
    page: string;
    initialSearchField: string;
    initialSearchType: SearchType;
    initialDateType: DateFilterTypes;
    initialDate: string;
    isSearchPending: boolean;
}

export interface State {
    autocomplete: AutocompleteItems;
    autocompleteStatus: AutocompleteStatuses;
    searchPending: boolean;
}

export type Props = StateProps;
