import {memo} from 'react';
import {withRouter} from 'react-router';

import Reports from './Reports';

export default withRouter(memo(Reports));
