import {FC} from 'react';

import {RouteProps} from '../../types';

export type Props = FC<RouteProps>;
