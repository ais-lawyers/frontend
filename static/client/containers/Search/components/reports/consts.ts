
export const DATE_FILTERS = {
    filter_date: 'за дату',
    filter_period: 'за период',
    filter_mounth: 'за месяц',
    filter_all: 'за все время',
}

export const SEARCH_TYPES = {
    filter_lawyer: 'по адвокату',
    filter_defendant: 'по подсудимому',
    filter_decision_date: 'по дате постановления',
    filter_all: 'по всей базе данных',
}
