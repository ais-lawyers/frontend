import {Typography} from 'antd';
import {FileExcelOutlined} from '@ant-design/icons';
import React, {useCallback} from 'react';
import {useSelector} from 'react-redux';

import Button from '__components/Button';

import {getSearchType, getDateType, getDate, getSearchField} from '../../selectors/router';
import {latestSearchSelector} from '../../selectors/searchFormSelectors';
import {paymentsExportService} from '../../services/paymentsExportService';
import {DATE_FILTERS, SEARCH_TYPES} from './consts';
import {Props} from './types';

import {b} from './Reports.scss';

const Reports: Props = (props) => {
    const data = useSelector(latestSearchSelector);
    const searchType = getSearchType(props);
    const dateType = getDateType(props);
    const date = getDate(props);
    const searchField= getSearchField(props);

    const onSaveReport = useCallback(
        async () => {
            await paymentsExportService.handleRequest({searchField, searchType, dateType, date});
        },
        [searchType, dateType, date, searchField],
    );

    return (
        <div className={b()}>
            <Typography.Title level={3}>
                Поиск {`${SEARCH_TYPES[data?.searchType]} ${data?.searchPhrase} ${DATE_FILTERS[data?.dateFilter]} ${data?.date ? data.date : ''}`}
            </Typography.Title>
            <Button type="primary" icon={<FileExcelOutlined/>} onClick={onSaveReport}>Сохранить в Excel</Button>
        </div>
    )
}

export default Reports;
