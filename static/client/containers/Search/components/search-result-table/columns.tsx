import {Tooltip, Button as VendorButton} from 'antd';
import {ColumnProps} from 'antd/lib/table';
import {DeleteOutlined, EditOutlined} from '@ant-design/icons';
import moment from 'moment';
import React from 'react';

import makeModel from '__utils/infrastructure/makeModel';
import {DEFAULT_DATE_FORMAT} from '__utils/date/defaultDateFormat';
import {isAdmin} from '__utils/isAdmin';

import Button from '__components/Button';

import {SearchItem, SearchType, DateFilterTypes} from '../../types';
import {mainSearchService} from '../../services/mainSearchService';

export const LOCALE = {
    emptyText: 'Нет пользователей',
}

const onConfirmDelete = (id: number, page: number, searchType: SearchType, dateType: DateFilterTypes, date: string, searchFieldValue: string) => async (event: React.MouseEvent) => {
    event.stopPropagation();
    await mainSearchService.delete(id, page, searchType, dateType, date, searchFieldValue);
}

export const getColumns = (page: number, searchType: SearchType, dateType: DateFilterTypes, date: string, searchFieldValue: string): ColumnProps<SearchItem>[] => ([
    {
        title: 'Адвокат',
        dataIndex: makeModel<SearchItem>(m => m.lawyer),
        key: makeModel<SearchItem>(m => m.lawyer),
        align: 'center',
    },
    {
        title: 'Образование',
        dataIndex: makeModel<SearchItem>(m => m.lawyer_education),
        key: makeModel<SearchItem>(m => m.lawyer_education),
        align: 'center',
    },
    {
        title: 'Подсудимый',
        dataIndex: makeModel<SearchItem>(m => m.defendant),
        key: makeModel<SearchItem>(m => m.defendant),
        align: 'center',
    },
    {
        title: 'Судья',
        dataIndex: makeModel<SearchItem>(m => m.judge),
        key: makeModel<SearchItem>(m => m.judge),
        align: 'center',
    },
    {
        title: 'Постановление',
        render: (_: unknown, {date}: SearchItem) => moment(date).format(DEFAULT_DATE_FORMAT),
        key: makeModel<SearchItem>(m => m.date),
        align: 'center',
    },
    {
        title: 'Сумма',
        render: (_: unknown, {cash}: SearchItem) =><span>{cash} &#8381;</span>,
        key: makeModel<SearchItem>(m => m.cash),
        align: 'center',
    },
    {
        title: 'Тип',
        dataIndex: makeModel<SearchItem>(m => m.type),
        key: makeModel<SearchItem>(m => m.type),
        align: 'center',
    },
    {
        title: 'Внесено',
        render: (_: unknown, {created_at}: SearchItem) => moment(created_at).format(DEFAULT_DATE_FORMAT),
        key: makeModel<SearchItem>(m => m.created_at),
        align: 'center',
    },
    {
        render: (_: unknown, {id}: SearchItem) => {
            const popconfirmOptionsUsers = {
                title: <span>Удалить запись</span>,
                okText: "Удалить",
                okType: "danger",
                cancelText: "Отмена",
                onConfirm: onConfirmDelete(id, page, searchType, dateType, date, searchFieldValue),
                show: true,
            }

            return (
                <VendorButton.Group>
                    <Tooltip title={isAdmin() ? 'Редактировать запись' : 'Редактирование доступно администратору'} placement="bottom">
                        <Button
                            icon={<EditOutlined/>}
                            disabled={!isAdmin()}
                        />
                    </Tooltip>

                    <Tooltip title={isAdmin() ? 'Удалить запись' : 'Удаление доступно администратору'} placement="bottom">
                        <div>
                            <Button
                                icon={<DeleteOutlined/>}
                                popconfirmoptions={isAdmin() ? popconfirmOptionsUsers : null}
                                onClick={evt => evt.stopPropagation()}
                                disabled={!isAdmin()}
                            />
                        </div>
                    </Tooltip>
                </VendorButton.Group>
            );
        },
        key: 'actions',
        align: 'center',
    },
]);
