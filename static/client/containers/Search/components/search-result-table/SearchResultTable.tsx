import React, {useCallback} from 'react';
import {Table} from 'antd';
import {useSelector} from 'react-redux';
import isEmpty from 'lodash/isEmpty';

import {bound as commonActions} from '__actions';
import {makeRowKey} from '__utils/makeRowKey';
import {isAdmin} from '__utils/isAdmin';

import {SearchItem} from '../../types';
import {makePath} from '../utils/makePath';
import {getSearchType, getDateType, getDate, getSearchField} from '../../selectors/router';
import {mainSearchSelector, mainSearchIsPendingSelector} from '../../selectors/mainSearchSelector';
import {getColumns} from './columns';
import {Props} from './types';

import {b} from './SearchResultTable.scss';

const SearchResultTable: Props = (props) => {
    const searchResults = useSelector(mainSearchSelector);
    const isSearchPending = useSelector(mainSearchIsPendingSelector);
    const searchType = getSearchType(props);
    const dateType = getDateType(props);
    const date = getDate(props);
    const searchFieldValue = getSearchField(props);

    const onPageClick = useCallback(
        (page: number) => {
            commonActions.router.push(makePath(page, searchFieldValue, searchType, dateType, date));
        },
        [searchType, dateType, date, searchFieldValue],
    );

    const onRowClick = useCallback(
        (page: number) => ({id}: SearchItem) => {
            if (!isAdmin()) {
                return null;
            } else {
                return ({
                    onClick: () => commonActions.router.push(`${makePath(page, searchFieldValue, searchType, dateType, date)}/edit/${id}`)
                });
            }
        },
        [searchType, dateType, date, searchFieldValue],
    );

    if (isEmpty(searchResults)) {
        return null;
    }

    const {data, page, total_items, page_size} = searchResults

    const columns = getColumns(page, searchType, dateType, date, searchFieldValue);

    return (
        <Table<SearchItem>
            bordered
            dataSource={data}
            rowKey={makeRowKey}
            loading={isSearchPending}
            columns={columns}
            pagination={
                {
                    total: total_items,
                    current: page,
                    pageSize: page_size,
                    hideOnSinglePage: true,
                    onChange: onPageClick,
                    showSizeChanger: false,
                }
            }
            onRow={onRowClick(page)}
            rowClassName={b('row')}
        />
    );
}

export default SearchResultTable;
