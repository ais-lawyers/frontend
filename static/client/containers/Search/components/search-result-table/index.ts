import {memo} from 'react';
import {withRouter} from 'react-router';

import SearchResultTable from './SearchResultTable';

export default withRouter(memo(SearchResultTable));
