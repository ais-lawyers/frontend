import {memo} from 'react';
import {compose} from 'redux';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import storeExtension from '__utils/hocs/storeExtension';
import asPage from '__utils/hocs/asPage';
import preload from '__utils/hocs/preload';

import {BundleState, RouteProps} from '../../types';
import search from '../../reducers';
import {mainSearchSelector} from '../../selectors/mainSearchSelector';
import {getPaymentsPage, getSearchField, getDate, getSearchType, getDateType, getPaymentId} from '../../selectors/router';
import Page from './Page';
import {onLoad} from './preloader';
import {StateProps} from './types';

const mapStateToProps = (state: BundleState, routerProps: RouteProps): StateProps => ({
    page: getPaymentsPage(routerProps),
    searchField: getSearchField(routerProps),
    searchType: getSearchType(routerProps),
    dateType: getDateType(routerProps),
    date: getDate(routerProps),
    paymentId: getPaymentId(routerProps),
    searchItems: mainSearchSelector(state),
});

export default compose(
    asPage({
        name: 'АИС Адвокаты | Поиск и отчеты',
        title: 'АИС Адвокаты | Поиск и отчеты'
    }),
    storeExtension<BundleState>({search}),
    withRouter,
    connect(mapStateToProps),
    preload({onLoad}),
)((Page));
