import React, {PureComponent} from 'react';

import {Modes} from '__types/form';

import PageHeader from '__components/PageHeader';

import EditForm from '../edit-form';
import SearchForm from '../search-form';
import SearchResultTable from '../search-result-table';
import Reports from '../reports';
import {Props} from './types';

import {b} from './Page.scss';

export default class Page extends PureComponent<Props> {
    public render() {
        const {mode} = this.props;

        return (
            <div className={b()}>
                <PageHeader title={<span>Поиск и отчеты</span>} subTitle="здесь можно воспользоваться поиском по БД и сформировать отчет"/>
    
                <SearchForm/>
    
                <Reports/>
    
                <SearchResultTable/>

                {mode === Modes.Edit && <EditForm/>}
            </div>
        );
    }
}
