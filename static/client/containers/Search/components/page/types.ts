import {Modes} from '__types/form';

import {RouteProps, SearchType, DateFilterTypes, MainSearchRequestResponse} from '../../types';

export interface OwnProps {
    mode: Modes;
}

export interface StateProps {
    page: string;
    paymentId: string;
    searchField: string;
    searchType: SearchType;
    dateType: DateFilterTypes;
    date: string;
    searchItems: MainSearchRequestResponse;
}

export type Props = RouteProps & StateProps & OwnProps;