import isEmpty from 'lodash/isEmpty';

import {Props} from './types';
import {mainSearchService} from '../../services/mainSearchService';

export function onLoad({page, searchField, searchType, date, dateType, paymentId, searchItems}: Props) {
    if (!paymentId || isEmpty(searchItems)) {
        mainSearchService.handleRequest({searchField, searchType, date, dateType}, page);
    }
}
