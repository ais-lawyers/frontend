import {IDType} from '__types/entity/api';

import {SearchType, DateFilterTypes} from '__containers/Search/types';

import {ROOT_PATH} from '../../consts';

export const makePath = (page: IDType, searchFieldValue: string, searchType: SearchType, dateType: DateFilterTypes, date = '""') => 
    `${ROOT_PATH}/${page}/${searchFieldValue}/${searchType}/${dateType}/${date}`;
