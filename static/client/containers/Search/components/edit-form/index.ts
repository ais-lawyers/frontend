import {ComponentType} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {compose} from 'redux';

import {BundleState, RouteProps} from '../../types';
import {getPaymentsPage, getSearchField, getSearchType, getDateType, getDate, getPaymentId} from '../../selectors/router';
import {mainSearchItemSelector, mainSearchIsPendingSelector} from '../../selectors/mainSearchSelector';
import EditForm from './EditForm';
import {StateProps} from './types';

const mapStateToProps = (state: BundleState, route: RouteProps): StateProps => {
    const paymentId = getPaymentId(route);

    return {
        paymentId: getPaymentId(route),
        page: getPaymentsPage(route),
        searchField: getSearchField(route),
        searchType: getSearchType(route),
        dateType: getDateType(route),
        date: getDate(route),
        searchItem: mainSearchItemSelector(paymentId)(state),
        isSearchPending: mainSearchIsPendingSelector(state),
    }
};

export default compose<ComponentType>(
    withRouter,
    connect(mapStateToProps),
)(EditForm);
