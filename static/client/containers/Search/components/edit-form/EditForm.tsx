import {
    Drawer,
    Form,
    Input,
    Tooltip,
    DatePicker,
    InputNumber,
    Select,
    Alert,
} from 'antd';
import {FormInstance} from 'antd/lib/form';
import {SaveOutlined} from '@ant-design/icons';
import React, {PureComponent} from 'react';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';

import {bound as commonActions} from '__actions';
import makeModel from '__utils/infrastructure/makeModel';
import {SKELETON} from '__utils/consts';
import {DEFAULT_DATE_FORMAT} from '__utils/date/defaultDateFormat';
import {lawyersPaymentsService} from '__services/LawyersPaymentsService';
import {CreatePaymentsRequest} from '__types/entity/payments/types';

import Button from '__components/Button';

import {makePath} from '../utils/makePath';
import {Props, Model, State} from './types';
import {SELECT_VALUES} from './consts';

import {b} from './EditForm.scss';

export default class EditForm extends PureComponent<Props, State> {
    public formRef = React.createRef<FormInstance>();

    public state: State = {
        isFieldsChanged: false,
        error: '',
    }

    public render() {
        return (
            <Drawer
            width="30vw"
            visible={true}
            onClose={this.onCloseForm}
            title="Редактировать запись"
            footer={this.renderFooter()}
            >
                {this.renderForm()}
            </Drawer>
        );
    }

    private onCloseForm = () => {
        const {page, searchField, searchType, dateType, date} = this.props;

        commonActions.router.push(makePath(page, searchField, searchType, dateType, date));
    }

    private renderForm = () => {
        const {searchItem, isSearchPending} = this.props;

        if (isSearchPending) {
            return SKELETON;
        }

        if (isEmpty(searchItem)) {
            return null;
        }

        const {lawyer, lawyer_education, defendant, judge, date, cash, type} = searchItem;

        const {error} = this.state;

        return (
            <Form
                onFieldsChange={this.onFieldsChange}
                layout="vertical"
                ref={this.formRef}
            >
                <Form.Item
                    name={makeModel<Model>(m => m.lawyer)}
                    initialValue={lawyer}
                    label="Адвокат:"
                    rules={[{required: true, message: 'Введите ФИО адвоката'}]}
                >
                    <Input placeholder="Б.Ю. Александров"/>
                </Form.Item>

                <Form.Item
                    name={makeModel<Model>(m => m.lawyer_education)}
                    initialValue={lawyer_education}
                    label="Образование:"
                    rules={[{required: true, message: 'Введите адвокатское образование'}]}
                >
                    <Input placeholder="Образование им. П.П. Кащенко"/>
                </Form.Item>

                <Form.Item
                    name={makeModel<Model>(m => m.defendant)}
                    initialValue={defendant}
                    label="Подсудимый:"
                    rules={[{required: true, message: 'Введите ФИО подсудимого'}]}
                >
                    <Input placeholder="Владимир Рудольфович Соловьев"/>
                </Form.Item>

                <Form.Item
                    name={makeModel<Model>(m => m.judge)}
                    initialValue={judge}
                    label="Судья:"
                    rules={[{required: true, message: 'Введите ФИО судьи'}]}
                >
                    <Input placeholder="Хахалева Елена Владимировна"/>
                </Form.Item>

                <Form.Item
                    name={makeModel<Model>(m => m.date)}
                    initialValue={moment(date)}
                    label="Дата постановления:"
                    rules={[{required: true, message: 'Выберите дату постановления'}]}
                >
                    <DatePicker
                        placeholder="06.06.06"
                        className={b('datepicker')}
                        format={DEFAULT_DATE_FORMAT}
                    />
                </Form.Item>

                <Form.Item
                    name={makeModel<Model>(m => m.cash)}
                    initialValue={cash}
                    label="Сумма ₽:"
                    rules={[{required: true, message: 'Введите сумму оплаты ₽'}]}
                >
                    <InputNumber placeholder="500" className={b('inputNumber')}/>
                </Form.Item>

                <Form.Item
                    name={makeModel<Model>(m => m.type)}
                    initialValue={this.makeTypeId(type)}
                    label="Тип дела:"
                    rules={[{required: true, message: 'Ввыберите тип дела'}]}
                >
                    <Select>
                        {SELECT_VALUES.map((value, key) => (
                            <Select.Option
                                key={key + value}
                                value={this.makeTypeId(value)}
                            >
                                {value}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>


                {error && (
                    <Alert
                        message="Ошибка"
                        description={error}
                        type="error"
                        showIcon
                    />
                )}
            </Form>
        );
    }

    private renderFooter = () => {
        const {isFieldsChanged} = this.state;

        return (
            <Tooltip title="Изменить запись">
                <Button
                    icon={<SaveOutlined/>}
                    type="primary"
                    htmlType="submit"
                    onClick={this.onSubmitForm}
                    disabled={!isFieldsChanged}
                >
                    Изменить
                </Button>
            </Tooltip>
        );
    }

    private onFieldsChange  = () => {
        this.setState({isFieldsChanged: true})
    }

    private onSubmitForm = async () => {
        const {paymentId} = this.props;

        const form = this.formRef.current;

        await form.validateFields()
            .then(async (values) => {
                const error = await lawyersPaymentsService.update(values as CreatePaymentsRequest, paymentId);

                if (error) {
                    error && this.setState({error});
                } else {
                    this.onCloseForm();
                }
            });
    }

    private makeTypeId = (type: string) => {
        switch (type) {
            case 'Гражданское':
                return 1;

            case 'Административное':
                return 2;

            case 'Уголовное':
            default:
                return 3;
        }
    }
}
