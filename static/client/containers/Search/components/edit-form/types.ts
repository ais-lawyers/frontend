import {SearchType, DateFilterTypes, MainSearchRequestResponse, RouteProps, SearchItem} from '../../types';

export interface StateProps {
    page: string;
    searchField: string;
    searchType: SearchType;
    dateType: DateFilterTypes;
    date: string;
    searchItem: SearchItem;
    isSearchPending: boolean;
    paymentId: string;
}

export type Props = StateProps & RouteProps;

export interface Model {
    lawyer: string;
    lawyer_education: string;
    defendant: string;
    judge: string;
    date: string;
    cash: string;
    type: 'Административное' | 'Гражданское' | 'Уголовное';
}

export interface State {
    isFieldsChanged: boolean;
    error: string;
}
