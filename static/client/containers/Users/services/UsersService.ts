import {notification} from 'antd';

import {bound as commonActions} from '__actions';
import {
    DEFAULT_RESPONSE_CATCH_MESSAGE,
    DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE,
} from '__utils/consts';

import {apiInstance} from '../api/UsersAPI';
import {USERS_PATH} from '../consts';
import {UpdateUserRequest} from '../types';

class UsersService {
    public update = async (data: UpdateUserRequest, userId: string, page: string) => {
        try {
            const requestData = data.password ? data : {username: data.username, role_id: data.role_id};

            const response = await apiInstance.update(requestData, userId);

            if (response.message.status === 'success') {
                notification.success({message: `Пользователь ${data.username} изменен`});

                commonActions.router.push(`${USERS_PATH}/${page}`)
            } else if (response.message.status === 'error') {
                notification.error({message: response.message.error});
                return response.message.error;
            }else {
                notification.error({message: DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE});
            }
        } catch (error) {
            notification.error({message: DEFAULT_RESPONSE_CATCH_MESSAGE});
        }
    }
}

export const usersService = new UsersService();
