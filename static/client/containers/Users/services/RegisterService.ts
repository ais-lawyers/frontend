import {notification} from 'antd';

import {bound as commonActions} from '__actions';

import {apiInstance} from '../api/RegisterAPI';
import {USERS_PATH} from '../consts';
import {
    DEFAULT_RESPONSE_CATCH_MESSAGE,
    DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE,
} from '__utils/consts';
import {CreateUserRequest} from '../types';

class RegisterService {
    public create = async (data: CreateUserRequest, page: string) => {
        try {
            const response = await apiInstance.create(data);

            if (response.message.status === 'success') {
                notification.success({message: `Пользователь ${data.username} создан`});

                commonActions.router.push(`${USERS_PATH}/${page}`)
            } else if (response.message.status === 'error') {
                notification.error({message: response.message.error});
                return response.message.error;
            }else {
                notification.error({message: DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE});
            }
        } catch (error) {
            notification.error({message: DEFAULT_RESPONSE_CATCH_MESSAGE});
        }
    }
}

export const registerService = new RegisterService();
