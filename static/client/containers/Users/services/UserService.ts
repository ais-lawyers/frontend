import {notification} from 'antd';
import {isEmpty} from 'lodash';

import {bound as commonActions} from '__actions';
import {IDType} from '__types/entity/api';

import {apiInstance} from '../api/UserAPI';
import {USERS_PATH} from '../consts';
import {
    DEFAULT_RESPONSE_CATCH_MESSAGE,
    DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE,
} from '__utils/consts';
import {usersLoader} from '../loaders/usersLoader';
import {UsersRequestResponse} from '../types';

class UserService {
    public delete = async (userId: IDType, page: IDType, username: string) => {
        try {
            const response = await apiInstance.delete(userId);

            if (response.message.status === 'success') {
                const {page: currentPage, users}: UsersRequestResponse = await usersLoader({page});

                notification.success({message: `Пользователь ${username} удален`});

                if (isEmpty(users)) {
                    if (page !== 1) {
                        commonActions.router.push(`${USERS_PATH}/${currentPage - 1}`);
                    }
                }
            } else if (response.message.status === 'error') {
                notification.error({message: response.message.error});
            }else {
                notification.error({message: DEFAULT_RESPONSE_BROKEN_INTERNET_MESSAGE});
            }
        } catch (error) {
            notification.error({message: DEFAULT_RESPONSE_CATCH_MESSAGE});
        }
    }
}

export const userService = new UserService();
