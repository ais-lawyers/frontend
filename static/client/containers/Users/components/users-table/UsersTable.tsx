import {Table} from 'antd';
import React from 'react';
import {useSelector} from 'react-redux';

import {bound as commonActions} from '__actions';
import {onPageClick} from '__utils/infrastructure/onPageClick';
import {makeRowKey} from '__utils/makeRowKey';
import {User} from '__types/entity/auth/types';

import {USERS_PATH} from '__containers/Users/consts';

import {usersSelector, usersIsPendingSelector} from '../../selectors/users';
import {getColumns, LOCALE} from './columns';
import {Props} from './types';

import {b} from './UsersTable.scss';

const onRowClick = (page: number) => ({id}: User) => ({
    onClick: () => commonActions.router.push(`${USERS_PATH}/${page}/edit/${id}`)
});

const UsersTable: Props = () => {
    const {users, total_items, page, page_size} = useSelector(usersSelector);
    const isUsersPending = useSelector(usersIsPendingSelector);

    const columns = getColumns(page);

    return (
        <Table
            bordered
            dataSource={users}
            columns={columns}
            locale={LOCALE}
            loading={isUsersPending}
            onRow={onRowClick(page)}
            rowClassName={b('userRow')}
            pagination={
                {
                    total: total_items,
                    current: page,
                    pageSize: page_size,
                    hideOnSinglePage: true,
                    onChange: onPageClick(`${USERS_PATH}`),
                }
            }
            rowKey={makeRowKey}
        />
    );
}

export default UsersTable;
