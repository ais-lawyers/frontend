import {Tooltip, Button as VendorButton} from 'antd';
import {ColumnProps} from 'antd/lib/table';
import {DeleteOutlined, EditOutlined} from '@ant-design/icons';
import moment from 'moment';
import React from 'react';

import makeModel from '__utils/infrastructure/makeModel';
import {makeRolesTags} from '__utils/infrastructure/mapRoles';

import Button from '__components/Button';

import {userService} from '../../services/UserService';
import {UserTableItem}  from './types';

export const LOCALE = {
    emptyText: 'Нет пользователей',
}

const onConfirmDelete = (id: number, page: number, username: string) => async (event: React.MouseEvent) => {
    event.stopPropagation();
    await userService.delete(id, page, username);
}

export const getColumns = (page: number): ColumnProps<UserTableItem>[] => ([
    {
        title: 'Имя пользователя',
        dataIndex: makeModel<UserTableItem>(m => m.username),
        key: makeModel<UserTableItem>(m => m.username),
        align: 'center',
    },
    {
        title: 'Пароль',
        render: () => <span>******</span>,
        key: 'password',
        align: 'center',
    },
    {
        title: 'Роль',
        render: (_: unknown, {role: {role}}: UserTableItem) => makeRolesTags(role),
        key: makeModel<UserTableItem>(m => m.role.role),
        align: 'center',
    },
    {
        title: 'Дата создания',
        render: (_: unknown, {created_at}: UserTableItem) =>
            moment(created_at).locale('ru').format('LLL'),
        key: makeModel<UserTableItem>(m => m.created_at),
        align: 'center',
    },
    {
        title: 'Последнее изменение',
        render: (_: unknown, {updated_at}: UserTableItem) =>
            moment(updated_at).locale('ru').format('LLL'),
        key: makeModel<UserTableItem>(m => m.updated_at),
        align: 'center',
    },
    {
        render: (_: unknown, {id, username}: UserTableItem) => {
            const isAdmin = username === 'admin';

            const tooltipTitle = isAdmin ? 'Нельзя удалить этого пользователя' : 'Удалить пользователя';

            const popconfirmOptionsUsers = {
                title: <span>Удалить пользователя <b>{username}</b></span>,
                okText: "Удалить",
                cancelText: "Отмена",
                okType: "danger",
                onConfirm: onConfirmDelete(id, page, username),
                show: true,
            }

            const popconfirmOptionsAdmin = {show: false}

            const popconfirmOptions = isAdmin ? popconfirmOptionsAdmin : popconfirmOptionsUsers;

            return (
                <VendorButton.Group>
                    <Tooltip title="Редактировать пользователя" placement="bottom">
                        <Button
                            icon={<EditOutlined/>}
                        />
                    </Tooltip>

                    <Tooltip title={tooltipTitle} placement="bottom">
                        <div>
                            <Button
                                icon={<DeleteOutlined/>}
                                popconfirmoptions={popconfirmOptions}
                                disabled={isAdmin}
                                onClick={evt => evt.stopPropagation()}
                            />
                        </div>
                    </Tooltip>
                </VendorButton.Group>
            );
        },
        key: 'actions',
        align: 'center',
    },
]);
