import  {FC} from 'react';

import {User} from '__types/entity/auth/types';

export type UserTableItem = User;

export type UsersTable = UserTableItem[];

export type Props = FC;
