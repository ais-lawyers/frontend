import {Modes} from '__types/form';

import {RouteProps, UsersRequestResponse} from '../../types';

export interface OwnProps {
    mode: Modes;
}

export interface StateProps {
    page: string;
    path: string;
    users: UsersRequestResponse;
}

export type Props = RouteProps & StateProps & OwnProps;
