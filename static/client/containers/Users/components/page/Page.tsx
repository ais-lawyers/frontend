import {Button, Tooltip} from 'antd';
import {PlusOutlined} from '@ant-design/icons';
import React from 'react';

import {bound as commonActions} from '__actions';
import {IDType} from '__types/entity/api';

import PageHeader from '__components/PageHeader';
import BlockedContent from '__components/BlockedContent';

import UsersTable from '../users-table';
import UserForm from '../user-form';
import {USERS_PATH} from '../../consts';
import {Props} from './types';
import { isAdmin } from '__utils/isAdmin';

const onCreateUserClick = (page: IDType) => () => {
    commonActions.router.push(`${USERS_PATH}/${page}/create`)
}

const PageHeaderTitle = (page: string) => {
    return (
        <React.Fragment>
            <span>Пользователи</span> &nbsp;
            <Tooltip title="Добавить пользователя">
                <Button
                    icon={<PlusOutlined/>}
                    type="primary"
                    onClick={onCreateUserClick(page || 1)}
                />
            </Tooltip>
        </React.Fragment>
    );
}

const Page = ({page, mode}: Props) => {
    if (!isAdmin()) {
        return <BlockedContent/>;
    }

    return(
        <section>
            <PageHeader title={PageHeaderTitle(page)} subTitle="здесь можно создать и редактировать пользователей"/>
            <UsersTable/>
            {mode && <UserForm mode={mode}/>}
        </section>
    );
}

export default Page;
