import {memo} from 'react';
import {compose} from 'redux';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import preload from '__utils/hocs/preload';
import storeExtension from '__utils/hocs/storeExtension';
import {pathnameSelector} from '__selectors/router';

import users from '../../reducers';
import {BundleState, RouteProps} from '../../types';
import {getUsersPage} from '../../selectors/router';
import {usersSelector} from '../../selectors/users';
import {onLoad} from './preloader';
import Page from './Page';
import {StateProps} from './types';
import asPage from '__utils/hocs/asPage';

const mapStateToProps = (state: BundleState, routerProps: RouteProps): StateProps => ({
    page: getUsersPage(routerProps),
    path: pathnameSelector(state),
    users: usersSelector(state),
});

export default compose(
    asPage({
        name: 'АИС Адвокаты | Пользователи',
        title: 'АИС Адвокаты | Пользователи',
    }),
    storeExtension({users}),
    withRouter,
    connect(mapStateToProps),
    preload({onLoad}),
)(memo(Page));
