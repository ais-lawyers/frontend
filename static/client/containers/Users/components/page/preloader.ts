import {isEmpty} from 'lodash';

import {usersLoader} from '../../loaders/usersLoader';
import {Props} from './types';

export function onLoad ({page, path, users}: Props) {
    if (!isEmpty(users) && (path.includes('/edit') || path.includes('/create'))) {
        return;
    }

    usersLoader({page});
}
