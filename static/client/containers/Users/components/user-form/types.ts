import {Modes} from '__types/form';
import {User} from '__types/entity/auth/types';

import {RouteProps} from '../../types';

export interface Model {
    username: string;
    password: string;
    password_confirmation: string;
    role_id: number;
};

export interface StateProps {
    userId: string;
    user: User;
    isUsersPending: boolean;
    page: string;
}

export interface OwnProps {
    mode: Modes;
}

export interface State {
    password: string;
    error: string;
    isFormChanged: boolean;
}

export type Props = OwnProps & RouteProps & StateProps;
