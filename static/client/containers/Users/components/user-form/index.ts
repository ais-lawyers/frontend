import {compose} from 'redux';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {ComponentType} from 'react';

import {BundleState, RouteProps} from '../../types';
import {getUserId, getUsersPage} from '../../selectors/router';
import {userSelector, usersIsPendingSelector} from '../../selectors/users';
import UserForm from './UserForm';
import {StateProps, OwnProps} from './types';

const mapStateToProps = (state: BundleState, routeProps: RouteProps): StateProps => {
    const userId = getUserId(routeProps);

    return {
        userId: getUserId(routeProps),
        user: userSelector(userId)(state),
        isUsersPending: usersIsPendingSelector(state),
        page: getUsersPage(routeProps),
    }
};

export default compose<ComponentType<OwnProps>>(
    withRouter,
    connect(mapStateToProps)
)(UserForm);
