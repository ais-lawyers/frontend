import {Drawer, Form, Input, Tooltip, Select, Alert} from 'antd';
import {FormInstance} from 'antd/lib/form';
import {
    UserOutlined,
    LockOutlined,
    SaveOutlined,
    PlusOutlined,
} from '@ant-design/icons';
import React, {PureComponent, ChangeEvent} from 'react';
import {isEmpty} from 'lodash';

import {bound as commonActions} from '__actions';
import makeModel from '__utils/infrastructure/makeModel';
import {SKELETON} from '__utils/consts';
import {ROLES} from '__utils/infrastructure/mapRoles';
import {Modes} from '__types/form';

import Button from '__components/Button';

import {registerService} from '../../services/RegisterService';
import {CreateUserRequest} from '../../types';
import {USERS_PATH} from '../../consts';
import {usersService} from '../../services/UsersService';
import {Props, Model, State} from './types';
import {EMPTY} from './consts';

const onCloseForm = (page: string) => () => {
    commonActions.router.push(`${USERS_PATH}/${page}`)
}

export default class UserForm extends PureComponent<Props, State> {
    public formRef = React.createRef<FormInstance>();

    public state: State = {
        password: '',
        error: '',
        isFormChanged: false,
    }

    public render () {
        const {user, page, mode} = this.props;

        return (
            <Drawer
                width="30vw"
                visible={true}
                onClose={onCloseForm(page)}
                title={mode === Modes.Edit ? `Редактировать пользователя ${user?.username}` : 'Создать пользователя'}
                footer={this.renderFooter()}
            >
                {this.renderForm()}
            </Drawer>
        );
    }

    private onSubmitForm = async () => {
        const form = this.formRef.current;

        await form.validateFields()
            .then(async (values) => {
                const {mode} = this.props;

                const isEdit = mode === Modes.Edit;

                if (isEdit) {
                    const {page, userId} = this.props;

                   await usersService.update(values as CreateUserRequest, userId, page);
                } else {
                    const {page} = this.props;

                    const error = await registerService.create(values as CreateUserRequest, page);

                    error && this.setState({error});
                }
            });
    }

    private renderFooter = () => {
        const {mode} = this.props;

        const {isFormChanged} = this.state;

        return (
            <Tooltip title={mode === Modes.Edit ? 'Изменить пользователя' : 'Создать пользователя'}>
                <Button
                    icon={mode === Modes.Create ? <PlusOutlined/> : <SaveOutlined/>}
                    type="primary"
                    htmlType="submit"
                    onClick={this.onSubmitForm}
                    disabled={!isFormChanged}
                >
                    {mode === Modes.Create ? 'Создать' : 'Изменить'}
                </Button>
            </Tooltip>
        );
    }

    private renderForm = () => {
        const {mode, isUsersPending, user} = this.props;
        const {password} = this.state;

        const isEdit = mode === Modes.Edit;

        if (isEdit && isUsersPending) {
            return SKELETON;
        }

        if (isEdit && !isUsersPending && isEmpty(user)) {
            return EMPTY;
        }

        const {error} = this.state;

        return (
            <Form
                name="user"
                layout="vertical"
                ref={this.formRef}
                onFieldsChange={this.onFormChanged}
                size="large"
            >
                <Form.Item
                    label="Имя пользователя"
                    name={makeModel<Model>(m => m.username)}
                    rules={[
                        {required: !isEdit},
                        {message: 'Введите имя пользователя'},
                        {max: 16, message: 'Максимум 16 символов'}
                    ]}
                    initialValue={user?.username}
                >
                    <Input
                        prefix={
                            <Tooltip title="Имя пользователя">
                                <UserOutlined className="site-form-item-icon"/>
                            </Tooltip>
                        }
                        disabled={user?.username === 'admin'}
                        placeholder="Б.Ю.Александров"
                    />
                </Form.Item>

                <Form.Item
                    label="Пароль"
                    name={makeModel<Model>(m => m.password)}
                    rules={[{required: !isEdit}, {min: 6, message: 'Минимум 6 символов'}]}
                >
                    <Input.Password
                        prefix={
                            <Tooltip title="Пароль пользователя">
                                <LockOutlined className="site-form-item-icon"/>
                            </Tooltip>
                        }
                        placeholder="Пароль"
                        onChange={this.onPasswordChange}
                    />
                </Form.Item>

                {password && (
                    <Form.Item
                        label="Подтверждение пароля"
                        name={makeModel<Model>(m => m.password_confirmation)}
                        rules={[{required: !isEdit}, {min: 6, message: 'Минимум 6 символов'}]}
                    >
                        <Input.Password
                            prefix={
                                <Tooltip title="Пароль пользователя">
                                    <LockOutlined className="site-form-item-icon"/>
                                </Tooltip>
                            }
                            placeholder="Пароль"
                            onChange={this.onConfirmationPasswordChange}
                        />
                    </Form.Item>
                )}

                <Form.Item
                    label="Роль"
                    name={makeModel<Model>(m => m.role_id)}
                    initialValue={user?.role.id}
                    rules={[{required: !isEdit}]}
                >
                    <Select
                        placeholder="Выберите роль пользователя"
                        disabled={user?.username === 'admin'}
                    >
                        {ROLES.map(({id, title}, key) => (
                            <Select.Option key={id + key} value={id}>
                                {title}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>

                {error && (
                    <Alert
                        message="Ошибка"
                        description={error}
                        type="error"
                        showIcon
                    />
                )}
            </Form>
        );
    }

    private onFormChanged = () => {
        this.setState({isFormChanged: true});
    }

    private onPasswordChange = (evt: ChangeEvent<HTMLInputElement>) => {
        this.setState({password: evt.target.value});

        this.onConfirmationPasswordChange();
    }

    private onConfirmationPasswordChange = () => {
        const {setFields, getFieldValue} = this.formRef.current;

        const password = getFieldValue(makeModel<Model>(m => m.password));
        const passwordConfirmation = getFieldValue(makeModel<Model>(m => m.password_confirmation));

        if (password !== passwordConfirmation) {
            setFields([
                {
                    errors: ['Пароли не совпадают'],
                    name: makeModel<Model>(m => m.password_confirmation),
                },
                {
                    errors: ['Пароли не совпадают'],
                    name: makeModel<Model>(m => m.password),
                },
            ]);
        } else {
            setFields([
                {
                    errors: [],
                    name: makeModel<Model>(m => m.password_confirmation),
                },
                {
                    errors: [],
                    name: makeModel<Model>(m => m.password),
                },
            ]);
        }
    }
}
