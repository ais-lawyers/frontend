import {items} from '../reducers/users';
import {entityLoader} from '__utils/infrastructure/reducers/entityLoader';

import {apiInstance} from '../api/UsersAPI';

export const usersLoader = entityLoader(apiInstance, {actions: items.actions});
