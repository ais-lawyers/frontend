import store from '__utils/infrastructure/store';

import {actions} from './reducers';

export const pure = {
    users: actions.users,
};
export const bound = store.bindActions(pure);
