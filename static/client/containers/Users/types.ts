import {RouteComponentProps} from 'react-router';

import {ExtendedState} from '__utils/infrastructure/store';
import {Pagination} from '__types/pagination';
import {Users} from '__types/entity/auth/types';
import {IDType} from '__types/entity/api';

import {UsersReducers} from './reducers';

export interface UsersRequest {
    page: IDType;
}

export interface UsersRequestResponse {
    page: number;
    page_size: number;
    total_items: number;
    total_pages: number;
    users: Users;
}

export interface CreateUserRequest {
    role: string;
    username: string;
    password: string;
    passsword_confirmation: string
};

export interface UpdateUserRequest {
    role_id?: string;
    username?: string;
    password?: string;
    passsword_confirmation?: string
};

export type BundleState = ExtendedState<{
    users: UsersReducers;
}>;

export interface RouterProps {
    page: string;
    userId: string;
}

export type RouteProps = RouteComponentProps<RouterProps>;
