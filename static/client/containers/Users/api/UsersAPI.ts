import {EntityAPI} from 'infrastructure/common';
import {BaseResponse} from 'core';

import {usersApi} from '__utils/transport';
import {IDType} from '__types/entity/api';

import {UsersRequestResponse, UsersRequest, UpdateUserRequest} from '../types';

class UsersAPI implements EntityAPI {
    public request = ({page}: UsersRequest) =>
        usersApi.get<UsersRequest, BaseResponse<UsersRequestResponse>>('', {page})
            .then(({data}) => data);

    public update = (data: UpdateUserRequest, userId: IDType) =>
        usersApi.put<UpdateUserRequest, BaseResponse<null>>(`/${userId}`, data)
            .then(response => response);
}

export const apiInstance = new UsersAPI();
