import {EntityAPI} from 'infrastructure/common';
import {BaseResponse} from 'core';

import {registerApi} from '__utils/transport';

import {CreateUserRequest} from '../types';

class RegisterAPI implements EntityAPI {
    public create = (data: CreateUserRequest) =>
        registerApi.post<CreateUserRequest, BaseResponse<any>>('', data)
            .then(response => response);
}

export const apiInstance = new RegisterAPI();
