import {EntityAPI} from 'infrastructure/common';
import {BaseResponse} from 'core';

import {IDType} from '__types/entity/api';
import {usersApi} from '__utils/transport';

class UserAPI implements EntityAPI {
    public delete = (userId: IDType) =>
        usersApi.delete<void, BaseResponse<null>>(`/${userId}`)
            .then(response => response);
}

export const apiInstance = new UserAPI();
