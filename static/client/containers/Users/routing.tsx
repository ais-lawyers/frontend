import React from 'react';
import Loadable from 'react-loadable';

import {Modes} from '__types/form';

import AppRoute from '__components/AppRoute';
import PageSpinner from '__components/PageSpinner';

import {USERS_PATH} from './consts';

const AsyncPage = Loadable({
    loader: () => import(/* webpackChunkName: "Users" */ './components/page'),
    loading: () => <PageSpinner/>,
} as Loadable.Options<unknown, never>);

export default (
    <React.Fragment>
        <AppRoute exact path={USERS_PATH} component={AsyncPage}/>

        <AppRoute exact path={`${USERS_PATH}/:page`} component={AsyncPage}/>

        <AppRoute
            exact
            path={`${USERS_PATH}/:page/create`}
            component={AsyncPage}
            componentProps={{
                mode: Modes.Create
            }}
        />

        <AppRoute
            exact
            path={`${USERS_PATH}/:page/edit/:userId`}
            component={AsyncPage}
            componentProps={{
                mode: Modes.Edit
            }}
        />
    </React.Fragment>
);
