import {combineReducers} from 'redux';

import {BaseEntityReducer} from '__utils/infrastructure/reducers/flow';

import {UsersRequestResponse} from '../types';
import {items as usersItems} from './users';

type UserReducer = BaseEntityReducer<UsersRequestResponse>;

export interface UsersReducers {
    users: UserReducer;
};

export const actions = {
    users: {
        items: usersItems.actions,
    },
};

export default combineReducers<UsersReducers>({
    users: combineReducers<UserReducer>({
        items: usersItems.reducer,
    }),
});
