import {generateEntityReducer} from '__utils/infrastructure/reducers/flow';

import { UsersRequestResponse } from '../types';

const PREFIX = '@users';

const {items} = generateEntityReducer<UsersRequestResponse>(PREFIX, {withItem: false});
export {items};
