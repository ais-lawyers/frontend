import {RouteProps} from '../types';

export const getUsersPage = (routeProps: RouteProps) =>
    routeProps.match.params?.page;

export const getUserId = (routeProps: RouteProps) =>
    routeProps.match.params?.userId;
