import {createSelector} from 'reselect';

import {checkPending} from '__utils/infrastructure/reducers/flow';

import {BundleState, UsersRequestResponse} from '../types';

export const usersSelector = createSelector(
    (state: BundleState) => state.users?.users?.items.data as UsersRequestResponse,
    (users: UsersRequestResponse) => (users || {}) as UsersRequestResponse,
);

export const usersIsPendingSelector = createSelector(
    (state: BundleState) => state.users?.users?.items.status,
    checkPending,
);

export const userSelector = (userId: string) => createSelector(
    usersSelector,
    (users: UsersRequestResponse) => {
        const user = users.users?.filter(({id}) => id === +userId)

        return user?.[0];
    },
);
