
import {Typography} from 'antd';
import {HomeOutlined} from '@ant-design/icons';
import React from 'react';

import RouteLink from '__components/RouteLink';

import {b} from './Page.scss';

const Page = () => {
    return (
        <section className={b()}>
            <Typography.Title>
                404
            </Typography.Title>

            <img src="/dist/images/404.png" alt="Страница не найдена"/>
            <span>Страница не найдена</span>

            <RouteLink url="/">
                Перейти на главную страницу &nbsp;
                <HomeOutlined/>
            </RouteLink>
        </section>
    );
}

export default Page;
