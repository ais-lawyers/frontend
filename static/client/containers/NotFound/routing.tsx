import React from 'react';
import Loadable from 'react-loadable';

import AppRoute from '__components/AppRoute';
import PageSpinner from '__components/PageSpinner';

const AsyncPage = Loadable({
    loader: () => import(/* webpackChunkName: "NotFoundPage" */ './components/page'),
    loading: () => <PageSpinner/>,
});

export default (
    <React.Fragment>
        <AppRoute component={AsyncPage}/>
    </React.Fragment>
);
