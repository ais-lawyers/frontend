import {memo} from 'react';

import asPage from '__utils/hocs/asPage';

import Page from './Page';

export default asPage({
    title: 'АИС Адвокаты | Главная',
    name: 'АИС Адвокаты | Главная',
})(memo(Page));
