import {Descriptions} from 'antd';
import React from 'react';

import PageHeader from '__components/PageHeader';

import {b} from './Page.scss';

const Page = () => {
    return (
        <section className={b()}>
            <PageHeader title={<span>Главная</span>} subTitle="информация о проекте"/>

            <div className={b('content')}>
                <Descriptions title="Технологический стэк" bordered>
                    <Descriptions.Item label="Version">3.0</Descriptions.Item>
                    <Descriptions.Item label="Frontend">React/Redux, Ant Design, Typescript</Descriptions.Item>
                    <Descriptions.Item label="SSR">Express</Descriptions.Item>
                    <Descriptions.Item label="Backend">Laravel</Descriptions.Item>
                    <Descriptions.Item label="Database">MySQL</Descriptions.Item>
                    <Descriptions.Item label="CI/CD">Gitlab</Descriptions.Item>
                    <Descriptions.Item label="backend-server">Open Server</Descriptions.Item>
                    <Descriptions.Item label="frontend-server">NodeJS</Descriptions.Item>
                </Descriptions>

                <Descriptions title="Функционал" bordered>
                    <Descriptions.Item label="Пользователи">создание, редактирование, удаление, разделение прав</Descriptions.Item>
                    <Descriptions.Item label="Оплата адвокату">создание, автокомплит полей, проверка на дубликаты в БД по параметрам</Descriptions.Item>
                    <Descriptions.Item label="Поиск и отчет">поиск по выбранным параметрам и генерация отчета, редактирование результатов</Descriptions.Item>
                </Descriptions>
            </div>
        </section>
    );
}

export default Page;
