import React from 'react';
import Loadable from 'react-loadable';

import AppRoute from '__components/AppRoute';
import PageSpinner from '__components/PageSpinner';

import {HOME_PATH} from './consts';

const AsyncPage = Loadable({
    loader: () => import(/* webpackChunkName: "Home" */ './components/page'),
    loading: () => <PageSpinner/>,
} as Loadable.Options<unknown, never>);

export default (
    <React.Fragment>
        <AppRoute exact path={HOME_PATH} component={AsyncPage}/>
    </React.Fragment>
);
